package pl.bdii.clinic.Registrar;

import java.io.IOException;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.hibernate.Session;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.Patient;
import pl.bdii.entities.Registrar;
import pl.bdii.util.HibernateUtil;


public class MainApp extends Application {

    public static Stage primaryStage;
    private static BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) {

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Rejestracja");


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        if(LoginPageController.loggedUser == null) {
            List<Registrar> RegistrarWizyt = session.createCriteria(Registrar.class).list();

            if (RegistrarWizyt.size() > 0)
                LoginPageController.loggedUser = RegistrarWizyt.get(0);

            session.getTransaction().commit();
            session.close();
        }

        initRootLayout();
        showMainScreen();
    }

    public void initRootLayout() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/pl/bdii/clinic/Registrar/BackStage.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showMainScreen() {
        try {


            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/pl/bdii/clinic/Registrar/MainScreen.fxml"));
            AnchorPane GlownyEkran = (AnchorPane) loader.load();

            rootLayout.setCenter(GlownyEkran);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void showAddPatient() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/pl/bdii/clinic/Registrar/AddPatient.fxml"));
            AnchorPane DodajPacjenta = (AnchorPane) loader.load();

            rootLayout.setCenter(DodajPacjenta);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showEditPatient(ActionEvent wydarzenie, Patient pacjent) {


        EditPatientControler edit = MainScreenControler.ChangeScene(wydarzenie, "/pl/bdii/clinic/Registrar/EditPatient.fxml", rootLayout);
        edit.setPatient(pacjent);
        edit.ini(pacjent);

    }


    public static void showNewVisit(ActionEvent wydarzenie, Patient pacjent) {

        NewVisitControler wizyta = MainScreenControler.ChangeScene(wydarzenie, "/pl/bdii/clinic/Registrar/NewVisit.fxml", rootLayout);
        wizyta.setPatient(pacjent);
        wizyta.ini();


    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
        HibernateUtil.shutdown();
    }
}
