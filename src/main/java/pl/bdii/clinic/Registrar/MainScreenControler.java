package pl.bdii.clinic.Registrar;

import java.util.List;

import javafx.scene.Node;
import javafx.stage.Stage;
import org.hibernate.Session;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Alert.AlertType;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.Patient;
import pl.bdii.entities.Registrar;
import pl.bdii.util.HibernateUtil;

public class MainScreenControler {


    static public <T> T ChangeScene(ActionEvent event, String path, BorderPane root) {
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(Object.class.getClass().getResource(path));
            Parent add_admin_page = (Parent) loader.load();


            root.setCenter(add_admin_page);


            return ((T) loader.getController());
        } catch (Exception ex) {
        }
        return null;
    }

    private Registrar user;

    private Patient choosenPatient = null;


    @FXML
    private TextField searchIdTextField;

    @FXML
    private TextField searchNameTextFile;

    @FXML
    private TextField searchSurrnameTextFile;

    @FXML
    private TextField searchPeselTextFile;

    @FXML
    private Label IDLabel;

    @FXML
    private Label nameLabel;

    @FXML
    private Label surrnameLabel;

    @FXML
    private Label PeselLabel;

    @FXML
    private Label NULabel;

    @FXML
    private Label DDLabel;

    @FXML
    private Label DOWLabel;

    @FXML
    private Label AdresLabel;

    public void InitializeTable() {

        user = (Registrar) LoginPageController.loggedUser;
    }

    private int IDNumber = -1;
    private String Name = "";
    private String SecoundName = "";
    private String PESELNumber = "";

    @FXML
    private void changeToNewVisit(ActionEvent event) {

        Patient pacjent = (Patient) patientTable.getSelectionModel().getSelectedItem();
        if (pacjent != null)
            choosenPatient = pacjent;
        else
            choosenPatient = null;
        if (choosenPatient == null) {
            System.out.print("Nie wybrano pacjenta\n");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Informacja");
            alert.setHeaderText(null);
            alert.setContentText("Proszę wybrać pacjenta!");
            alert.showAndWait();
        } else {


            MainApp.showNewVisit(event, choosenPatient);
        }
    }

    @FXML
    private void changeAddPatient() {
        MainApp.showAddPatient();
    }


    @FXML
    private void exitButton(ActionEvent actionEvent) {
        Stage app_stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        app_stage.close();
        HibernateUtil.shutdown();
        System.out.print("Nastapilo wylogowanie\n");
    }

    @FXML
    private TableView patientTable;



    @FXML
    private void searchPatient() {
        System.out.print("Szukanie\n");

        TableColumn<Patient, Integer> IDPacjent = new TableColumn<>("ID");
        IDPacjent.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("id"));

        TableColumn<Patient, String> ImiePacjent = new TableColumn<>("Imię");
        ImiePacjent.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));

        TableColumn<Patient, String> NazwiskoPacjent = new TableColumn<>("Nazwisko");
        NazwiskoPacjent.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));

        TableColumn<Patient, String> PeselPacjent = new TableColumn<>("PESEL");
        PeselPacjent.setCellValueFactory(new PropertyValueFactory<Patient, String>("pesel"));

        patientTable.getColumns().setAll(IDPacjent, ImiePacjent, NazwiskoPacjent, PeselPacjent);
        ObservableList<Patient> lista = FXCollections.observableArrayList();

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Patient> ListaPacjentow = session.createCriteria(Patient.class).list();

        if (searchIdTextField.getText().isEmpty())
            IDNumber = -1;
        else
            IDNumber = Integer.parseInt(searchIdTextField.getText());

        if (searchNameTextFile.getText().isEmpty())
            Name = "";
        else
            Name = searchNameTextFile.getText();

        if (searchSurrnameTextFile.getText().isEmpty())
            SecoundName = "";
        else
            SecoundName = searchSurrnameTextFile.getText();

        if (searchPeselTextFile.getText().isEmpty())
            PESELNumber = "";
        else
            PESELNumber = searchPeselTextFile.getText();

        boolean nieSpelniaWymagan = false;

        for (Patient entry : ListaPacjentow) {
            nieSpelniaWymagan = false;
            entry.getAdres().getCity();
            if (IDNumber != -1)
                if (entry.getId() != IDNumber)
                    nieSpelniaWymagan = true;
            if (!Name.equals(""))
                if (!entry.getFirstName().matches("^"+Name+".*"))
                    nieSpelniaWymagan = true;
            if (!SecoundName.equals(""))
                if (!entry.getLastName().matches("^"+SecoundName+".*"))
                    nieSpelniaWymagan = true;
            if (!PESELNumber.equals(""))
                if (!entry.getPesel().matches("^"+PESELNumber+"[0-9]*"))
                    nieSpelniaWymagan = true;
            ;

            if (nieSpelniaWymagan != true) {
                lista.add(entry);
            }
        }


        session.getTransaction().commit();
        session.close();


        patientTable.setItems(lista);

    }


    @FXML
    private void choosePatient() {
        Patient pacjent = (Patient) patientTable.getSelectionModel().getSelectedItem();
        if (pacjent != null) {
            IDLabel.setText(Integer.toString(pacjent.getId()));
            nameLabel.setText(pacjent.getFirstName());
            surrnameLabel.setText(pacjent.getLastName());
            PeselLabel.setText(pacjent.getPesel());
            NULabel.setText(pacjent.getUbezpiecznieNumber());
            AdresLabel.setText(pacjent.getAdres().getCity() + " ul. " + pacjent.getAdres().getStreetName() + " " + pacjent.getAdres().getHouseNumber() + "/" + pacjent.getAdres().getFlatNumber() + ", " + pacjent.getAdres().getPostalCode());
        }
    }


    @FXML
    private void editPatient(ActionEvent event) {

        Patient pacjent = (Patient) patientTable.getSelectionModel().getSelectedItem();
        if (pacjent != null)
            choosenPatient = pacjent;
        else
            choosenPatient = null;
        if (choosenPatient == null) {
            System.out.print("Nie wybrano pacjenta\n");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Informacja");

            alert.setContentText("Proszę wybrać pacjenta!");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else {

            MainApp.showEditPatient(event, choosenPatient);
        }
    }


    @FXML
    private void deletePatient() {

        Patient pacjent = (Patient) patientTable.getSelectionModel().getSelectedItem();
        if (pacjent != null) {
            System.out.print("Usuwanie\n");
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            session.delete(pacjent);

            session.getTransaction().commit();
            session.close();
            searchPatient();


        }
    }


}
