package pl.bdii.clinic.Registrar;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import pl.bdii.entities.Adres;
import pl.bdii.entities.Patient;
import pl.bdii.util.HibernateUtil;

public class AddPatientControler {

    @FXML
    private void changMainScreen() {
        MainApp.showMainScreen();
    }

    public List<?> getAdres(Class<?> cl, Session ses) {

        List<?> adresList = ses.createCriteria(cl).list();

        return adresList;
    }


    @FXML
    private TextField NameTextField;

    @FXML
    private TextField SurrnameTextField;

    @FXML
    private TextField PeselTextField;

    @FXML
    private TextField NUTextField;

    @FXML
    private TextField CityTextField;

    @FXML
    private TextField StreetTextField;

    @FXML
    private TextField NDTextField;

    @FXML
    private TextField NLTextField;

    @FXML
    private TextField KPTextField;

    @FXML
    private void addPatient() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            if (NameTextField.getText().isEmpty() || SurrnameTextField.getText().isEmpty() || PeselTextField.getText().isEmpty() || CityTextField.getText().isEmpty() || StreetTextField.getText().isEmpty() || NUTextField.getText().isEmpty() || KPTextField.getText().isEmpty() || NDTextField.getText().isEmpty() || NLTextField.getText().isEmpty()) {
                System.out.print("Braki w danych\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setTitle("Informacja");
                alert.setContentText("Proszę uzupełnić wszystkie komórki!");
                alert.showAndWait();
            } else {


                String first = NameTextField.getText();
                String last = SurrnameTextField.getText();
                String pesel = PeselTextField.getText();
                String miejscowosc = CityTextField.getText();
                String ulica = StreetTextField.getText();
                String nrUbez = NUTextField.getText();
                int dom = Integer.parseInt(NDTextField.getText());
                int lokal = Integer.parseInt(NLTextField.getText());
                String postal = KPTextField.getText();



                if(!pesel.matches("^[0-9]{11}$"))
                    throw new Exception();

                if(!postal.matches("^[0-9]{2}-[0-9]{3}$"))
                throw new Exception();



                    List<Adres> results = new ArrayList<Adres>();
                results = (List<Adres>) getAdres(Adres.class, session);

                Adres nowyAdres = new Adres(miejscowosc, ulica, postal, dom, lokal, null);
                boolean wystepuje = false;

                for (Adres Obj : results) {
                    if (nowyAdres.getCity().equals(Obj.getCity()))
                        if (nowyAdres.getStreetName().equals(Obj.getStreetName()))
                            if (nowyAdres.getHouseNumber() == Obj.getHouseNumber())
                                if (nowyAdres.getFlatNumber() == Obj.getFlatNumber())
                                    if (nowyAdres.getPostalCode().equals(Obj.getPostalCode())) {
                                        nowyAdres = Obj;
                                        wystepuje = true;

                                        break;
                                    }

                }

                if (wystepuje) {
                    System.out.print("Podany adres istnieje\n");
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Informacja");
                    alert.setContentText("Podany adres już istnieje!");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                } else {
                    session.save(nowyAdres);
                }
                session.save(new Patient(first, last, pesel, nowyAdres, nrUbez));

                session.getTransaction().commit();
                session.close();


                System.out.print("Dodano nowego Pacjenta\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Informacja");
                alert.setHeaderText(null);
                alert.setContentText("Dodano nowego pacjenta!");
                alert.showAndWait();

                MainApp.showMainScreen();
            }
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setContentText("Błędne dane!");
            alert.setTitle("Blad");
            alert.showAndWait();
        }
    }


}
