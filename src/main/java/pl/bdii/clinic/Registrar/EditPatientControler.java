package pl.bdii.clinic.Registrar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.bdii.entities.Adres;
import pl.bdii.entities.Patient;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pl.bdii.util.STATUS;

public class EditPatientControler {

    private Patient patient;

    @FXML
    private TableView VisitTab;

    @FXML
    private TextField NameTextField;

    @FXML
    private TextField SurrnameTextField;

    @FXML
    private TextField PeselTextField;

    @FXML
    private TextField NUTextField;

    @FXML
    private TextField CityTextField;

    @FXML
    private TextField StreetTextField;

    @FXML
    private TextField NDTextField;

    @FXML
    private TextField NLTextField;

    @FXML
    private TextField KPTextField;


    public void ini(Patient pacjent) {
        NameTextField.setText(pacjent.getFirstName());
        SurrnameTextField.setText(pacjent.getLastName());
        PeselTextField.setText(pacjent.getPesel());
        NUTextField.setText(pacjent.getUbezpiecznieNumber());
        CityTextField.setText(pacjent.getAdres().getCity());
        StreetTextField.setText(pacjent.getAdres().getStreetName());
        NDTextField.setText(Integer.toString(pacjent.getAdres().getHouseNumber()));
        NLTextField.setText(Integer.toString(pacjent.getAdres().getFlatNumber()));
        KPTextField.setText(pacjent.getAdres().getPostalCode());

        TableColumn<Visit, Date> DataWizyty = new TableColumn<>("Data Wizyty");
        DataWizyty.setCellValueFactory(new PropertyValueFactory<Visit, Date>("visitDate"));

        TableColumn<Visit, Date> planAnulDate = new TableColumn<>("Data Zmiany Statusu");
        planAnulDate.setCellValueFactory(new PropertyValueFactory<Visit, Date>("planCancleDT"));

        TableColumn<Visit, String> StatusWizyty = new TableColumn<>("Status");
        StatusWizyty.setCellValueFactory(new PropertyValueFactory<Visit, String>("statusName"));

        TableColumn<Visit, String> ImieNazwiskoLekarza = new TableColumn<>("Lekarz");
        ImieNazwiskoLekarza.setCellValueFactory(new PropertyValueFactory<Visit, String>("imieNazwiskoLekarza"));

        VisitTab.getColumns().setAll(DataWizyty,ImieNazwiskoLekarza, StatusWizyty,planAnulDate);
        ObservableList<Visit> lista = FXCollections.observableArrayList();

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Visit> ListaWizyt = session.createCriteria(Visit.class).list();

        for (Visit entry : ListaWizyt) {
            if (entry.getPatient().getId() == patient.getId()) {
                entry.imieNazwiskoLekarza = entry.getDoctor().getFirstName() + " " + entry.getDoctor().getLastName();
                entry.getPatient().getFirstName();

                switch (entry.getStatus()) {
                    case STATUS.WIZYTY.CANCLED:
                        entry.statusName="ANULOWANA";
                        break;
                    case STATUS.WIZYTY.ENDED:
                        entry.statusName="ZAKOŃCZONA";
                        break;
                    case STATUS.WIZYTY.REGISTRED:
                        entry.statusName="ZAREJESTROWANA";
                        break;
                }

                lista.add(entry);
            }
        }


        session.getTransaction().commit();
        session.close();

        if (lista == null) {
            System.out.println("Brak wizyt");
        } else {
            VisitTab.setItems(lista);
        }


    }

    public void setPatient(Patient pacjent) {
        this.patient = pacjent;
    }

    @FXML
    private void changeMainScreen() {
        MainApp.showMainScreen();
    }


    public List<?> getAdres(Class<?> cl, Session ses) {
        @SuppressWarnings("deprecation")
        List<?>
                adresList = ses.createCriteria(cl).list();

        return adresList;
    }

    @FXML
    private void editPatient() {

        try {

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();


            if (NameTextField.getText().isEmpty() || SurrnameTextField.getText().isEmpty() || PeselTextField.getText().isEmpty() || CityTextField.getText().isEmpty() || StreetTextField.getText().isEmpty() || NUTextField.getText().isEmpty() || KPTextField.getText().isEmpty() || NDTextField.getText().isEmpty() || NLTextField.getText().isEmpty()) {
                System.out.print("Braki w danych\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Informacja");
                alert.setHeaderText(null);
                alert.setContentText("Proszę uzupełnić wszystkie komórki!");
                alert.showAndWait();
            } else {

                String first = NameTextField.getText();
                String last = SurrnameTextField.getText();
                String pesel = PeselTextField.getText();
                String miejscowosc = CityTextField.getText();
                String ulica = StreetTextField.getText();
                String nrUbez = NUTextField.getText();
                int dom = Integer.parseInt(NDTextField.getText());
                int lokal = Integer.parseInt(NLTextField.getText());
                String postal = KPTextField.getText();

                if(!pesel.matches("^[0-9]{11}$"))
                    throw new Exception();

                if(!postal.matches("^[0-9]{2}-[0-9]{3}$"))
                    throw new Exception();

                List<Adres> results = new ArrayList<Adres>();
                results = (List<Adres>) getAdres(Adres.class, session);

                Adres nowyAdres = new Adres(miejscowosc, ulica, postal, dom, lokal, null);
                boolean wystepuje = false;

                for (Adres Obj : results) {
                    if (nowyAdres.getCity().equals(Obj.getCity()))
                        if (nowyAdres.getStreetName().equals(Obj.getStreetName()))
                            if (nowyAdres.getHouseNumber() == Obj.getHouseNumber())
                                if (nowyAdres.getFlatNumber() == Obj.getFlatNumber())
                                    if (nowyAdres.getPostalCode().equals(Obj.getPostalCode())) {
                                        wystepuje = true;
                                        nowyAdres = Obj;
                                        break;
                                    }

                }

                if (wystepuje) {
                    System.out.print("Podany adres istnieje\n");
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Informacja");
                    alert.setHeaderText(null);
                    alert.setContentText("Podany adres już istnieje!");
                    alert.showAndWait();
                } else {
                    session.save(nowyAdres);
                }
                patient.setAdres(nowyAdres);
                patient.setFirstName(first);
                patient.setLastName(last);
                patient.setPesel(pesel);
                patient.setUbezpiecznieNumber(nrUbez);

                session.update(patient);

                session.getTransaction().commit();
                session.close();


                System.out.print("Edytowano Pacjenta\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Informacja");
                alert.setHeaderText(null);
                alert.setContentText("Edytowano pacjenta!");
                alert.showAndWait();

                MainApp.showMainScreen();
            }
        }
        catch(Exception ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setContentText("Błędne dane!");
            alert.setTitle("Blad");
            alert.showAndWait();
        }
    }

    @FXML
    private void deleteVisit() {

        Visit wizyta = (Visit) VisitTab.getSelectionModel().getSelectedItem();
        if (wizyta == null) {
            System.out.print("Nie wybrano wizyty\n");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Informacja");
            alert.setHeaderText(null);
            alert.setContentText("Proszę wybrać wizytę!");
            alert.showAndWait();
        } else {
            if (wizyta.getStatus().equals(STATUS.WIZYTY.CANCLED)) {
                //od-anulujemy
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();
                wizyta.setStatus(STATUS.WIZYTY.REGISTRED);
                wizyta.setPlanCancleDT(null);
                session.update(wizyta);

                session.getTransaction().commit();
                session.close();


                System.out.print("Od-Anulowano wizyte\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Informacja");
                alert.setHeaderText(null);
                alert.setContentText("Przywrócono wizytę!");
                alert.showAndWait();
            } else {
                //anulujemy
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                wizyta.setStatus(STATUS.WIZYTY.CANCLED);
                wizyta.setPlanCancleDT(new Date());
                session.update(wizyta);

                session.getTransaction().commit();
                session.close();

                System.out.print("Anulowano wizyte\n");
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Informacja");
                alert.setHeaderText(null);
                alert.setContentText("Anulowano wizytę!");
                alert.showAndWait();

            }
            TableColumn<Visit, Date> DataWizyty = new TableColumn<>("Data Wizyty");
            DataWizyty.setCellValueFactory(new PropertyValueFactory<Visit, Date>("visitDate"));

            TableColumn<Visit, String> StatusWizyty = new TableColumn<>("Status");
            StatusWizyty.setCellValueFactory(new PropertyValueFactory<Visit, String>("statusName"));

            TableColumn<Visit, String> ImieNazwiskoLekarza = new TableColumn<>("Lekarz");
            ImieNazwiskoLekarza.setCellValueFactory(new PropertyValueFactory<Visit, String>("imieNazwiskoLekarza"));

            TableColumn<Visit, Date> planAnulDate = new TableColumn<>("Data Zmiany Statusu");
            planAnulDate.setCellValueFactory(new PropertyValueFactory<Visit, Date>("planCancleDT"));

            VisitTab.getColumns().setAll(DataWizyty,ImieNazwiskoLekarza, StatusWizyty,planAnulDate);
            ObservableList<Visit> lista = FXCollections.observableArrayList();

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Visit> ListaWizyt = session.createCriteria(Visit.class).list();

            for (Visit entry : ListaWizyt) {
                if (entry.getPatient().getId() == patient.getId()) {
                    entry.imieNazwiskoLekarza = entry.getDoctor().getFirstName() + " " + entry.getDoctor().getLastName();
                    entry.getPatient().getFirstName();

                    switch (entry.getStatus()) {
                        case STATUS.WIZYTY.CANCLED:
                            entry.statusName="ANULOWANA";
                            break;
                        case STATUS.WIZYTY.ENDED:
                            entry.statusName="ZAKONCZONA";
                            break;
                        case STATUS.WIZYTY.REGISTRED:
                            entry.statusName="ZAREJESTROWANA";
                            break;
                    }

                    lista.add(entry);
                }
            }


            session.getTransaction().commit();
            session.close();

            if (lista == null) {
                System.out.println("Brak wizyt");
            } else {
                VisitTab.setItems(lista);
            }
        }


    }

}
