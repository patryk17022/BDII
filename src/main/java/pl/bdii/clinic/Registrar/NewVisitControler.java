package pl.bdii.clinic.Registrar;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javafx.scene.control.*;
import org.hibernate.Session;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.Doctor;
import pl.bdii.entities.Patient;
import pl.bdii.entities.Registrar;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import javafx.scene.control.Alert.AlertType;
import pl.bdii.util.STATUS;

public class NewVisitControler {

    private Patient actualPatient;
    private Registrar user;

    @FXML
    private Label IDPacjentaLabel;

    @FXML
    private Label IDRecepLabel;

    @FXML
    private ChoiceBox IDLekaChoiceBox;

    @FXML
    private DatePicker dateDP;

    @FXML
    private TextField TimeTextField;

    public void setPatient(Patient pacj) {
        actualPatient = pacj;
    }

    public void ini() {
        IDPacjentaLabel.setText(Integer.toString(actualPatient.getId()) + " - " + actualPatient.getLastName());
        user = (Registrar) LoginPageController.loggedUser;
        if (user != null)
            IDRecepLabel.setText(Integer.toString(user.getId()) + " - " + user.getLastName());

        LocalDate today = LocalDate.now();
        dateDP.setValue(today);

        ObservableList<String> lista = FXCollections.observableArrayList();

        lista.add("Wybierz lekarza");

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Doctor> ListaLekarzy = session.createCriteria(Doctor.class).list();

        for (Doctor lekarz : ListaLekarzy) {
            if (lekarz.getDeactivationDate() == null)
                lista.add(Integer.toString(lekarz.getId()) + " - " + lekarz.getLastName());
        }

        session.getTransaction().commit();
        session.close();


        IDLekaChoiceBox.setValue("Wybierz lekarza");
        IDLekaChoiceBox.setItems(lista);

    }

    @FXML
    private void changeMainScreen() {
        MainApp.showMainScreen();
    }

    @FXML
    private void addVisit() {

        String wybor = (String) IDLekaChoiceBox.getSelectionModel().getSelectedItem();

        if (wybor.equals("Wybierz lekarza")) {
            System.out.print("Nie wybrano lekarza\n");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Informacja");
            alert.setHeaderText(null);
            alert.setContentText("Proszę wybrać lekarza z listy!");
            alert.showAndWait();
        } else {


            Calendar calendar = Calendar.getInstance();
            Date kiedyDodano = new Date();
            Date kiedyMaSieOdbyc;
            Doctor lekarz = null;
            Patient pacjent = actualPatient;
            int statusPoczatkowy = 0;
            String deparsk = "";
            kiedyDodano = calendar.getTime();


            Date dateset = java.sql.Date.valueOf(dateDP.getValue());
            calendar.setTime(dateset);
            //TimeTextField = "12:30"
            int godzina = 0;
            int minuta = 0;
            try {
                deparsk = "";
                deparsk = String.valueOf(TimeTextField.getText().charAt(0)) + String.valueOf(TimeTextField.getText().charAt(1));
                godzina = Integer.parseInt(deparsk);
                deparsk = "";
                deparsk = String.valueOf(TimeTextField.getText().charAt(3)) + String.valueOf(TimeTextField.getText().charAt(4));
                minuta = Integer.parseInt(deparsk);
            } catch (Exception ex) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("Niepoprawna godzina!");
                alert.showAndWait();
                return;
            }

            Date now = java.sql.Date.valueOf(LocalDate.now());

            if (dateset.compareTo(now) < 0) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Data pochodzi z przeszłości!");
                alert.showAndWait();
                return;
            }

            if (!(godzina >= 0 && godzina <= 24) || !(minuta >= 0 && minuta <= 60)) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Niepoprawna godzina!");
                alert.setHeaderText(null);
                alert.showAndWait();
                return;
            }

            calendar.set(calendar.get(calendar.YEAR), calendar.get(calendar.MONTH), calendar.get(calendar.DAY_OF_MONTH), godzina, minuta, 00);
            kiedyMaSieOdbyc = calendar.getTime();

            //lekarz - szukamy po ID

            int index = 0;
            deparsk = "";
            while (wybor.charAt(index) != ' ') {
                deparsk = deparsk + wybor.charAt(index);
                index++;
            }
            index = Integer.parseInt(deparsk);
            //index = ID wybranego lekarza

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Doctor> ListaLekarzy = session.createCriteria(Doctor.class).list();
            for (Doctor doc : ListaLekarzy) {
                if (doc.getId() == index) {
                    lekarz = doc;
                    break;
                }
            }

            Visit dodawanaWizyta = new Visit(null, null, STATUS.WIZYTY.REGISTRED, kiedyMaSieOdbyc, kiedyDodano, lekarz, pacjent, null, null, null);

            session.save(dodawanaWizyta);

            session.getTransaction().commit();
            session.close();


            System.out.print("Dodano nowa Wizyte\n");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Informacja");
            alert.setHeaderText(null);
            alert.setContentText("Dodano nową wizytę!");
            alert.showAndWait();

            MainApp.showMainScreen();
        }
    }


}
