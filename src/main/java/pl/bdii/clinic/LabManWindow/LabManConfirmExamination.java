/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabManWindow;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.hibernate.Session;
import pl.bdii.clinic.LabTechWindow.LabTechWindow;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.LabManager;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;


public class LabManConfirmExamination {


    private ExaminationLab exam;

    @FXML
    private TextArea results;

    @FXML
    private TextArea doctorinfo;

    @FXML
    private TextArea labInfo;

    @FXML
    private Button cancleButton;

    @FXML
    private Button acceptButton;

    @FXML
    private Label info;

    public void ini(ExaminationLab exam) {
        this.exam = exam;
        results.setText(exam.getResult());
        doctorinfo.setText(exam.getDocAnn());
        info.setText("Badanie: " + exam.getId());
        labInfo.setText(exam.getLabManAnn());

        if(!exam.getStatus().equals(STATUS.BADANIA.PERFORMED))
        {
            acceptButton.setDisable(true);
            labInfo.setEditable(false);
        }

        if(exam.getStatus().equals(STATUS.BADANIA.CANCLED))
        {
            cancleButton.setDisable(true);
        }

    }

    @FXML
    public void acceptButton(ActionEvent event) throws IOException {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        exam.setStatus(STATUS.BADANIA.CONFIRMED);
        LabManager user = (LabManager) LoginPageController.loggedUser;
        exam.setLabMan(user);
        exam.setLabManAnn(labInfo.getText());

        session.update(exam);

        session.getTransaction().commit();
        session.close();

        LabManWindow anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManWindow.fxml");
        anu.InitializeTable();
    }

    @FXML
    public void cancleButton(ActionEvent event) throws IOException {

        ExaminationLab lab = exam;
        if (lab != null) {
            LabManCancelExamination anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManCancle.fxml");
            anu.ini(lab);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie wybrano badania !");
            alert.showAndWait();
        }


    }

    @FXML
    public void backButton(ActionEvent event) throws IOException {

        LabManWindow anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManWindow.fxml");
        anu.InitializeTable();

    }

}
