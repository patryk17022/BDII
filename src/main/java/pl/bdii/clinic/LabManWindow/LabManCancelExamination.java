/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabManWindow;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.hibernate.Session;
import pl.bdii.clinic.LabTechWindow.LabTechWindow;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.LabManager;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;


public class LabManCancelExamination {

    private ExaminationLab exam;

    @FXML
    private Label idExam;

    public void ini(ExaminationLab exam) {
        this.exam = exam;
        idExam.setText("ID: " + Integer.toString(exam.getId()));
    }

    @FXML
    public void onClickNOButton(ActionEvent event) throws IOException {
        LabManWindow anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManWindow.fxml");
        anu.InitializeTable();
    }


    @FXML
    public void onClickYesButton(ActionEvent event) throws IOException {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        exam.setStatus(STATUS.BADANIA.CANCLED);
        LabManager user = (LabManager) LoginPageController.loggedUser;
        exam.setLabMan(user);
        session.update(exam);

        session.getTransaction().commit();
        session.close();

        LabManWindow anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManWindow.fxml");
        anu.InitializeTable();

    }
}
