/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabManWindow;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.clinic.LabTechWindow.LabTechWindow;
import pl.bdii.entities.ExaminationDictionary;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.util.List;

public class LabManWindow {

    @FXML
    private TableView table;

    @FXML
    private TextField idFil;

    @FXML
    private ComboBox typeFil;

    @FXML
    private ComboBox statusFil;


    private boolean init = false;

    public void filterButton(ActionEvent event)
    {


        InitializeTable();
    }

    public void initializeCombo(){
        init= true;

        ObservableList<String> daneStatus = FXCollections.observableArrayList();

        daneStatus.add("WSZYSTKIE");
        daneStatus.add("NEW - NOWA");
        daneStatus.add("PER - PRZEPROWADZONA");
        daneStatus.add("CON - ZAKONCZONE");
        daneStatus.add("CAN - ANULOWANE");

        statusFil.setItems(daneStatus);
        statusFil.getSelectionModel().select(2);

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                .add(Restrictions.eq("type", "LAB")).list();

        ObservableList<String> dane = FXCollections.observableArrayList();

        dane.add("WSZYSTKIE");
        for (ExaminationDictionary entry : obiektyzBazy) {
            dane.add(entry.getName());

        }

        session.getTransaction().commit();
        session.close();


        typeFil.setItems(dane);
        typeFil.getSelectionModel().selectFirst();




    }

    public void InitializeTable() {

        if(!init)
            initializeCombo();

        TableColumn<ExaminationLab, Integer> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<ExaminationLab, Integer>("id"));

        TableColumn<ExaminationLab, String> docAnn = new TableColumn<>("Informacje od Doktora");
        docAnn.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("docAnn"));

        TableColumn<ExaminationLab, String> result = new TableColumn<>("Wynik");
        result.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("result"));

        TableColumn<ExaminationLab, String> nameExam = new TableColumn<>("Typ badania");
        nameExam.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("nameExam"));

        TableColumn<ExaminationLab, String> Status = new TableColumn<>("Status");
        Status.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("status"));

        table.getColumns().setAll(id, docAnn, result, nameExam,Status);

        ObservableList<ExaminationLab> data = FXCollections.observableArrayList();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationLab> list = session.createCriteria(ExaminationLab.class).list();

        for (ExaminationLab entry : list) {


            if(!statusFil.getSelectionModel().isSelected(0) && !entry.getStatus().equals(((String)(statusFil.getSelectionModel().getSelectedItem())).substring(0,3)))
                continue;

            try {
                if (idFil.getText().length() != 0 && entry.getId() != Integer.parseInt(idFil.getText()))
                    continue;
            }catch(Exception ex)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setContentText("Nie poprawne dane!");
                alert.setTitle("Blad");
                alert.showAndWait();
            }

            if(!typeFil.getSelectionModel().isSelected(0) && !entry.getExDic().getName().equals(((String)(typeFil.getSelectionModel().getSelectedItem()))))
                continue;


            entry.setNameExam(entry.getExDic().getName());
            data.add(entry);
        }


        session.getTransaction().commit();
        session.close();

        table.setItems(data);
    }


    @FXML
    public void confirmButton(ActionEvent event) throws IOException {
        ExaminationLab lab = (ExaminationLab) table.getSelectionModel().getSelectedItem();
        if (lab != null) {
            LabManConfirmExamination anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabManWindow/LabManConfirm.fxml");
            anu.ini(lab);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie wybrano badania !");
            alert.showAndWait();
        }

    }




    @FXML
    public void exBut(ActionEvent event) {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.close();
        HibernateUtil.shutdown();
    }
}
