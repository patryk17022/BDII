package pl.bdii.clinic.AdministratorWindow;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.bdii.util.HibernateUtil;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/pl/bdii/clinic/AdministratorWindow/adminWindow.fxml"));
        Parent add_admin_page = (Parent) loader.load();
        ((adminWindowController) loader.getController()).InitializeTable();
        primaryStage.setTitle("Panel Administracji");
        primaryStage.setScene(new Scene(add_admin_page));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
        HibernateUtil.shutdown();
    }
}
