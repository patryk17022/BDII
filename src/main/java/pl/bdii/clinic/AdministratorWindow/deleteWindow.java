package pl.bdii.clinic.AdministratorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import pl.bdii.entities.User;
import pl.bdii.util.HibernateUtil;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class deleteWindow {

    @FXML
    private Label nameUsr;

    private User user;

    public void SetName(User user) {
        this.user = user;
        nameUsr.setText(user.getFirstName() + " " + user.getLastName());
    }

    @SuppressWarnings("deprecation")
    @FXML
    public void pressYesButtton(ActionEvent event) throws IOException, HibernateException, ClassNotFoundException {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Class<?> cl = Class.forName("pl.bdii.entities." + user.getRola());

        List<?> obj = session.createCriteria(cl).add(Restrictions.eq("firstName", user.getFirstName()))
                .add(Restrictions.eq("lastName", user.getLastName())).add(Restrictions.eq("login", user.getLogin()))
                .list();

        ((User)obj.get(0)).setDeactivationDate(new Date());
        session.update(obj.get(0));

        session.getTransaction().commit();
        session.close();

        System.out.println("Użytkownik " + user.getFirstName() + " " + user.getLastName() + " został deaktywowany!");

        adminWindowController.ChangeScene(event, InfoWindow.CreateInfo(getClass(), "Użytkownik został deaktywowany !"),
                "Info");

    }

    @FXML
    public void pressNoButtton(ActionEvent event) throws IOException {
        adminWindowController.goToAdminScene(getClass(), event);
    }

}
