package pl.bdii.clinic.AdministratorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pl.bdii.entities.*;
import pl.bdii.util.HibernateUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import javax.xml.soap.Text;

public class adminWindowController {

    @FXML
    public TableView<User> usersTab;

    public List<?> getUsers(Class<?> cl, Session ses) {
        @SuppressWarnings("deprecation")
        List<?> usresList = ses.createCriteria(cl).list();

        for (Object Obj : usresList) {
            User usr = (User) Obj;
            usr.setRola(cl.getSimpleName());
        }

        return usresList;
    }

    @FXML
    private TextField nameTB;

    @FXML
    private TextField surrnameTB;

    @FXML
    private TextField loginTB;

    @FXML
    private CheckBox activeTB;

    @FXML
    private ComboBox roleTB;

    private boolean init = false;




    @FXML
    public void filterButton(ActionEvent event)
    {
        InitializeTable();
    }

    private void initialiceComboBox(){
        init =true;

        ObservableList<String> daneStatus = FXCollections.observableArrayList();

        daneStatus.add("WSZYSTKIE");
        daneStatus.add("Doctor - Doktor");
        daneStatus.add("Registrar - Rejestrator");
        daneStatus.add("LabManager - Kierownik Laboratorium");
        daneStatus.add("LabTechnician - Laborant");
        daneStatus.add("Admin - Administrator");

        roleTB.setItems(daneStatus);
        roleTB.getSelectionModel().selectFirst();

    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    public void InitializeTable() {

        if(!init)
            initialiceComboBox();

        TableColumn<User, String> loginCol = new TableColumn<>("Login");
        loginCol.setMinWidth(150);
        loginCol.setCellValueFactory(new PropertyValueFactory<User, String>("login"));

        TableColumn<User, String> firstNameCol = new TableColumn<>("Imie");
        firstNameCol.setMinWidth(150);
        firstNameCol.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));

        TableColumn<User, String> secondNameCol = new TableColumn<>("Nazwisko");
        secondNameCol.setMinWidth(150);
        secondNameCol.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));

        TableColumn<User, String> roleCol = new TableColumn<>("Rola");
        roleCol.setMinWidth(150);
        roleCol.setCellValueFactory(new PropertyValueFactory<User, String>("rola"));

        TableColumn<User, String> deactivationDate = new TableColumn<>("Data deaktywacji");
        deactivationDate.setMinWidth(150);
        deactivationDate.setCellValueFactory(new PropertyValueFactory<User, String>("deactivationDate"));

        usersTab.getColumns().setAll(loginCol, firstNameCol, secondNameCol, roleCol,deactivationDate);

        System.out.println("Pobrano z bazy użytkowników");

        ObservableList<User> data = FXCollections.observableArrayList();

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<?> results = new ArrayList<User>();

        results = getUsers(Admin.class, session);
        results.addAll((List) getUsers(Doctor.class, session));
        results.addAll((List) getUsers(LabManager.class, session));
        results.addAll((List) getUsers(LabTechnician.class, session));
        results.addAll((List) getUsers(Registrar.class, session));

        for (Object obj : results) {

            User usr = (User) obj;
            String role = (String)(roleTB.getSelectionModel().getSelectedItem());
            if(!roleTB.getSelectionModel().isSelected(0) && !usr.getRola().equals(role.substring(0,role.indexOf(" "))))
                continue;

            if (nameTB.getText().length() != 0 && !usr.getFirstName().toUpperCase().matches("^"+nameTB.getText().toUpperCase()+".*"))
                continue;
            if (surrnameTB.getText().length() != 0 && !usr.getLastName().toUpperCase().matches("^"+surrnameTB.getText().toUpperCase()+".*"))
                continue;
            if (loginTB.getText().length() != 0 && !usr.getLogin().toUpperCase().matches("^"+loginTB.getText().toUpperCase()+".*"))
                continue;

            if(usr.getDeactivationDate() == null && activeTB.isSelected() == false || usr.getDeactivationDate() != null && activeTB.isSelected() == true )
                continue;



            data.add(usr);
        }

        session.getTransaction().commit();
        session.close();

        usersTab.setItems(data);
    }

    static public void ChangeScene(ActionEvent event, Scene scene, String title) {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setTitle(title);
        app_stage.setScene(scene);
        app_stage.show();
    }

    static public void goToAdminScene(Class<?> myClass, ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(myClass.getResource("/pl/bdii/clinic/AdministratorWindow/adminWindow.fxml"));
        Parent add_admin_page = (Parent) loader.load();
        ((adminWindowController) loader.getController()).InitializeTable();

        Scene add_admin_scene = new Scene(add_admin_page);

        adminWindowController.ChangeScene(event, add_admin_scene, "Panel Administracji");
    }

    @FXML
    public void pressAddUserButton(ActionEvent actionEvent) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/pl/bdii/clinic/AdministratorWindow/addUser.fxml"));
        Parent add_user_page = (Parent) loader.load();

        ((AddUserController) loader.getController()).InitializeChoicebox();

        Scene add_user_scene = new Scene(add_user_page);
        ChangeScene(actionEvent, add_user_scene, "Dodawanie użytkownika");

    }

    @FXML
    public void pressChangeButton(ActionEvent actionEvent) throws IOException, ClassNotFoundException {

        User chooseanUser = (User) usersTab.getSelectionModel().getSelectedItem();
        if (chooseanUser != null) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/pl/bdii/clinic/AdministratorWindow/changeRoleWindow.fxml"));
            Parent add_confirm_page = (Parent) loader.load();
            ((ChangeRoleWindow) loader.getController()).InitializeChoicebox();
            ((ChangeRoleWindow) loader.getController()).SetUser(chooseanUser);
            Scene add_confirm_scene = new Scene(add_confirm_page);
            adminWindowController.ChangeScene(actionEvent, add_confirm_scene, "Modyfikacja Roli");
        } else {
            adminWindowController.ChangeScene(actionEvent,
                    InfoWindow.CreateInfo(getClass(), "Nie wybrano użytkownika!"), "Error");
        }

    }

    @FXML
    public void pressDeleteButton(ActionEvent actionEvent) throws IOException {
        User chooseanUser = (User) usersTab.getSelectionModel().getSelectedItem();

        if(chooseanUser.getDeactivationDate() != null)
        {
            adminWindowController.ChangeScene(actionEvent,
                    InfoWindow.CreateInfo(getClass(), "Użytkownik został już deaktywowany!"), "Error");
        }

        if (chooseanUser != null) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/pl/bdii/clinic/AdministratorWindow/deleteWindow.fxml"));
            Parent add_confirm_page = (Parent) loader.load();
            ((deleteWindow) loader.getController()).SetName(chooseanUser);
            Scene add_confirm_scene = new Scene(add_confirm_page);
            adminWindowController.ChangeScene(actionEvent, add_confirm_scene, "Deaktywacja");
        } else {
            adminWindowController.ChangeScene(actionEvent,
                    InfoWindow.CreateInfo(getClass(), "Nie wybrano użytkownika!"), "Error");
        }
    }

    @FXML
    public void pressBackButton(ActionEvent actionEvent) {
        Stage app_stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        app_stage.close();
        HibernateUtil.shutdown();
    }
}
