package pl.bdii.clinic.AdministratorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;

import java.io.IOException;

public class InfoWindow {

    @FXML
    private Label name;

    public void SetName(String nameUser) {
        name.setText(nameUser);
    }

    @FXML
    private void pressOkButton(ActionEvent event) throws IOException {
        adminWindowController.goToAdminScene(getClass(), event);

    }

    public static Scene CreateInfo(Class<?> oClass, String text) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(oClass.getResource("/pl/bdii/clinic/AdministratorWindow/InfoWindow.fxml"));
        Parent add_page = (Parent) loader.load();
        ((InfoWindow) loader.getController()).SetName(text);
        Scene add_scene = new Scene(add_page);
        return add_scene;
    }

}
