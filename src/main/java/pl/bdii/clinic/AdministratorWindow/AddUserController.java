package pl.bdii.clinic.AdministratorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.exception.ConstraintViolationException;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.*;
import pl.bdii.util.HibernateUtil;
import javafx.collections.FXCollections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class AddUserController {
    @FXML
    private RadioButton doctor;

    @FXML
    private Label incorrect;

    @FXML
    private TextField userName;

    @FXML
    private TextField loginName;

    @FXML
    private TextField userSurname;

    @FXML
    private TextField password;

    @FXML
    private TextField password2;

    @FXML
    private ToggleGroup Rola;

    @FXML
    private Label spec_lab;

    @SuppressWarnings("rawtypes")
    @FXML
    private ChoiceBox spec;

    @FXML
    private void pressCancleButton(ActionEvent event) throws IOException {
        adminWindowController.goToAdminScene(getClass(), event);
    }

    @FXML
    private void checkRoleStat(ActionEvent event) throws IOException {
        RadioButton chk = (RadioButton) Rola.getSelectedToggle();
        String roleId = chk.idProperty().get();
        if (roleId.equals("doctor")) {
            System.out.println("DOCTOR");
            spec_lab.setVisible(true);
            spec.setVisible(true);
        } else {

            spec.setVisible(false);
            spec_lab.setVisible(false);
        }
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public void InitializeChoicebox() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<?> obj = session.createCriteria(Specialty.class).list();

        List<String> list = new ArrayList<String>();
        for (Object objec : obj) {
            list.add(((Specialty) objec).getName());

        }

        session.getTransaction().commit();
        session.close();

        spec.setItems(FXCollections.observableArrayList(list));
        spec.getSelectionModel().selectFirst();
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    @FXML
    private void pressAddButton(ActionEvent event) throws IOException {
        RadioButton chk = (RadioButton) Rola.getSelectedToggle();
        String roleId = chk.idProperty().get();

        try {

            if (password.getText().isEmpty() == false && password.getText().equals(password2.getText())) {
                if (userName.getText().isEmpty() == true || userSurname.getText().isEmpty() == true
                        || loginName.getText().isEmpty() == true) {

                    Alert errorAlert = new Alert(AlertType.ERROR);
                    errorAlert.setHeaderText("Wprowadzono niepoprawne dane.");
                    errorAlert.setTitle("Niepoprawne dane");

                    errorAlert.showAndWait();
                    return;
                }
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                String first = userName.getText();
                String last = userSurname.getText();
                String login = loginName.getText();
                String pass = LoginPageController.encodedUserPassword(password.getText());

                switch (roleId) {
                    case "admin":
                        session.save(new Admin(first, last, pass, login));
                        break;
                    case "doctor":
                        if (spec != null) {
                            String spec_name = spec.getSelectionModel().getSelectedItem().toString();
                            List<Specialty> obj = session.createCriteria(Specialty.class)
                                    .add(Restrictions.eq("name", spec_name)).list();

                            session.save(new Doctor(obj.get(0), first, last, pass, login));
                        }

                        break;
                    case "registrar":
                        session.save(new Registrar(first, last, pass, login));
                        break;
                    case "labManager":
                        session.save(new LabManager(first, last, pass, login));
                        break;
                    case "labTechnician":
                        session.save(new LabTechnician(first, last, pass, login));
                        break;
                    default:
                        System.out.println("error");

                }

                session.getTransaction().commit();
                session.close();

                System.out.println("Użytkownik: " + userName.getText() + " " + userSurname.getText() + " w roli "
                        + ((RadioButton) doctor.getToggleGroup().getSelectedToggle()).getText() + " został dodany!");

                adminWindowController.ChangeScene(event,
                        InfoWindow.CreateInfo(getClass(),
                                "Użytkownik: " + userName.getText() + " " + userSurname.getText() + " w roli "
                                        + ((RadioButton) doctor.getToggleGroup().getSelectedToggle()).getText()
                                        + " został dodany!"),
                        "Dodano");
            } else {
                incorrect.setVisible(true);
            }
        }
        catch(Exception ex)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano typu lub login nie jest unikalny!");
            alert.showAndWait();
        }
    }
}
