package pl.bdii.clinic.AdministratorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.*;
import pl.bdii.util.HibernateUtil;
import javafx.collections.FXCollections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class ChangeRoleWindow {
    @FXML
    private RadioButton doctor;

    @FXML
    private RadioButton admin;

    @FXML
    private RadioButton labManager;

    @FXML
    private RadioButton labTechnician;

    @FXML
    private RadioButton registrar;

    @FXML
    private Label incorrect;

    @FXML
    private TextField userName;

    @FXML
    private TextField loginName;

    @FXML
    private TextField userSurname;

    @FXML
    private TextField password;

    @FXML
    private TextField password2;

    @FXML
    private ToggleGroup Rola;

    @FXML
    private Label spec_lab;

    @SuppressWarnings("rawtypes")
    @FXML
    private ChoiceBox spec;

    User user;
    Class<?> cl;

    @FXML
    private void pressCancleButton(ActionEvent event) throws IOException {
        adminWindowController.goToAdminScene(getClass(), event);
    }

    @FXML
    private void checkRoleStat(ActionEvent event) throws IOException {

        CheckRS();

    }

    private void CheckRS() {
        RadioButton chk = (RadioButton) Rola.getSelectedToggle();
        String roleId = chk.idProperty().get();
        if (roleId.equals("doctor")) {
            spec_lab.setVisible(true);
            spec.setVisible(true);
        } else {
            spec_lab.setVisible(false);
            spec.setVisible(false);
        }
    }

    @FXML
    private Button bt_add;

    @SuppressWarnings("unchecked")
    public void SetUser(User user) throws ClassNotFoundException {


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        this.user = user;
        cl = Class.forName("pl.bdii.entities." + user.getRola());

        userName.setText(user.getFirstName());
        loginName.setText(user.getLogin());
        userSurname.setText(user.getLastName());

        if (cl == Doctor.class) {
            @SuppressWarnings("deprecation")
            List<?> obj = session.createCriteria(cl).add(Restrictions.eq("firstName", user.getFirstName()))
                    .add(Restrictions.eq("lastName", user.getLastName())).add(Restrictions.eq("login", user.getLogin()))
                    .list();
            spec.getSelectionModel().select(((Doctor) obj.get(0)).getSpecialty().getName());
        }

        RadioButton chk = (RadioButton) Rola.getSelectedToggle();
        chk.setSelected(false);

        switch (cl.getSimpleName()) {
            case "Admin":
                admin.setSelected(true);
                break;
            case "Doctor":
                doctor.setSelected(true);
                break;
            case "Registrar":
                registrar.setSelected(true);
                break;
            case "LabManager":
                labManager.setSelected(true);
                break;
            case "LabTechnician":
                labTechnician.setSelected(true);
                break;
            default:
                System.out.println("error");

        }
        CheckRS();
        session.getTransaction().commit();
        session.close();

        if(user.getDeactivationDate() != null) {
            bt_add.setDisable(false);
            spec.setDisable(false);
            userName.setEditable(false);
            userSurname.setEditable(false);
            password.setEditable(false);
            password2.setEditable(false);
        }

        admin.setDisable(true);
        doctor.setDisable(true);
        registrar.setDisable(true);
        labManager.setDisable(true);
        labTechnician.setDisable(true);

    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public void InitializeChoicebox() {


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<?> obj = session.createCriteria(Specialty.class).list();
        System.out.println("INICIALIZE ChoicBox");
        List<String> list = new ArrayList<String>();
        for (Object objec : obj) {
            list.add(((Specialty) objec).getName());

        }

        session.getTransaction().commit();
        session.close();

        spec.setItems(FXCollections.observableArrayList(list));
        spec.getSelectionModel().selectFirst();
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    @FXML
    private void pressAddButton(ActionEvent event) throws IOException {
        RadioButton chk = (RadioButton) Rola.getSelectedToggle();
        String roleId = chk.idProperty().get();

        try {

            if (password.getText().equals(password2.getText()) && password.getText().isEmpty() == false) {

                if (userName.getText().isEmpty() == true || userSurname.getText().isEmpty() == true
                        || loginName.getText().isEmpty() == true) {

                    Alert errorAlert = new Alert(AlertType.ERROR);
                    errorAlert.setTitle("Niepoprawne dane");
                    errorAlert.setHeaderText("Wprowadzono niepoprawne dane.");
                    errorAlert.showAndWait();
                    return;
                }

                String first = userName.getText();
                String last = userSurname.getText();
                String login = loginName.getText();
                String pass = LoginPageController.encodedUserPassword(password.getText());
                String spec_name = spec.getSelectionModel().getSelectedItem().toString();

                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                List<?> del = session.createCriteria(cl).add(Restrictions.eq("firstName", user.getFirstName()))
                        .add(Restrictions.eq("lastName", user.getLastName())).add(Restrictions.eq("login", user.getLogin()))
                        .list();

                if (cl.toString().toUpperCase().equals(("class pl.bdii.entities." + roleId).toUpperCase())) {
                    ((User) del.get(0)).setFirstName(first);
                    ((User) del.get(0)).setLastName(last);
                    ((User) del.get(0)).setLogin(login);
                    ((User) del.get(0)).setPassword(pass);

                    if (cl.toString().toUpperCase().equals(("class pl.bdii.entities.doctor").toUpperCase())) {
                        List<Specialty> obj = session.createCriteria(Specialty.class).add(Restrictions.eq("name", spec_name))
                                .list();
                        ((Doctor) del.get(0)).setSpecialty(obj.get(0));
                    }
                    session.update(del.get(0));
                    session.getTransaction().commit();
                    session.close();
                    System.out.println("Modyfikacja dokonana!");
                    adminWindowController.ChangeScene(event, InfoWindow.CreateInfo(getClass(), "Modyfikacja dokonana!"), "Dodano");
                    return;
                }
                session.delete(del.get(0));

                session.getTransaction().commit();
                session.beginTransaction();


                switch (roleId) {
                    case "admin":
                        session.save(new Admin(first, last, pass, login));
                        break;
                    case "doctor":

                        List<Specialty> obj = session.createCriteria(Specialty.class).add(Restrictions.eq("name", spec_name))
                                .list();

                        session.save(new Doctor(obj.get(0), first, last, pass, login));

                        break;
                    case "registrar":
                        session.save(new Registrar(first, last, pass, login));
                        break;
                    case "labManager":
                        session.save(new LabManager(first, last, pass, login));
                        break;
                    case "labTechnician":
                        session.save(new LabTechnician(first, last, pass, login));
                        break;
                    default:
                        System.out.println("error");

                }

                session.getTransaction().commit();
                session.close();

                System.out.println("Modyfikacja dokonana!");

                adminWindowController.ChangeScene(event, InfoWindow.CreateInfo(getClass(), "Modyfikacja dokonana!"),
                        "Dodano");
            } else {
                incorrect.setVisible(true);
            }
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano typu!");
            alert.showAndWait();
        }

    }
}
