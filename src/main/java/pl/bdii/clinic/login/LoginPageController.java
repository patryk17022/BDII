package pl.bdii.clinic.login;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import pl.bdii.clinic.AdministratorWindow.adminWindowController;
import pl.bdii.clinic.DoctorWindow.DoctorWindow;
import pl.bdii.clinic.LabManWindow.LabManWindow;
import pl.bdii.clinic.LabTechWindow.LabTechWindow;
import pl.bdii.clinic.Registrar.MainApp;
import pl.bdii.entities.Admin;
import pl.bdii.entities.Doctor;
import pl.bdii.entities.LabManager;
import pl.bdii.entities.LabTechnician;
import pl.bdii.entities.Registrar;
import pl.bdii.entities.User;
import pl.bdii.util.HibernateUtil;

public class LoginPageController {

    Stage stage;
    @FXML
    ToggleGroup role;
    @FXML
    TextField firstNameTextField;
    @FXML
    TextField passwordTextField;

    public static Object loggedUser;

    Alert errorAlert = new Alert(AlertType.ERROR);

    public LoginPageController() {

        errorAlert.setTitle("LOGIN INFO");
        errorAlert.setHeaderText("Niepoprawne dane.");
    }

    public static String encodedUserPassword(String pass) {

        String encodedPassword = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] arr = md.digest(pass.getBytes());
            encodedPassword = Base64.getEncoder().encodeToString(arr);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encodedPassword;
    }

    @FXML
    private void handleLoginButtonAction(ActionEvent event) throws NoSuchAlgorithmException, IOException {
        System.out.println("Login try.");

        RadioButton chk = (RadioButton) role.getSelectedToggle();
        String roleId = chk.idProperty().get();
        Stage primaryStage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        switch (roleId) {
            case "Admin":
                if (loginTryAuthentication(Admin.class) == true) {
                    loader.setLocation(getClass().getResource("/pl/bdii/clinic/AdministratorWindow/adminWindow.fxml"));
                    Parent add_admin_page = (Parent) loader.load();
                    ((adminWindowController) loader.getController()).InitializeTable();
                    primaryStage.setTitle("Panel Administracji");
                    primaryStage.setScene(new Scene(add_admin_page));
                    primaryStage.show();
                    stage.close();
                }
                break;
            case "Doctor":
                if (loginTryAuthentication(Doctor.class) == true) {
                    loader.setLocation(getClass().getResource("/pl/bdii/clinic/DoctorWindow/DoctorWindow.fxml"));
                    Parent add_doctor_page = (Parent) loader.load();
                    ((DoctorWindow) loader.getController()).InitializeTable();
                    primaryStage.setTitle("Panel Doktora");
                    primaryStage.setScene(new Scene(add_doctor_page));
                    primaryStage.show();
                    stage.close();
                }
                break;
            case "Registrar":
                if (loginTryAuthentication(Registrar.class) == true) {
                    MainApp maintest = new MainApp();
                    maintest.start(primaryStage);
                    stage.close();
                }
                break;
            case "LabManager":
                if (loginTryAuthentication(LabManager.class) == true) {
                    loader.setLocation(getClass().getResource("/pl/bdii/clinic/LabManWindow/LabManWindow.fxml"));
                    Parent add_doctor_page = (Parent) loader.load();
                    ((LabManWindow) loader.getController()).InitializeTable();
                    primaryStage.setTitle("Panel Kierownika");
                    primaryStage.setScene(new Scene(add_doctor_page));
                    primaryStage.show();
                    stage.close();
                }
                break;
            case "LabTechnician":
                if (loginTryAuthentication(LabTechnician.class) == true) {
                    loader.setLocation(getClass().getResource("/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml"));
                    Parent add_doctor_page = (Parent) loader.load();
                    ((LabTechWindow) loader.getController()).InitializeTable();
                    primaryStage.setTitle("Panel Laboranta");
                    primaryStage.setScene(new Scene(add_doctor_page));
                    primaryStage.show();
                    stage.close();
                }
                break;
            default:
                errorAlert.showAndWait();


        }


    }

    public void showScene(Parent parent, Stage stage) {

        this.stage = stage;
        Scene scene = new Scene(parent);

        stage.setTitle("Logowanie do systemu przychodni");
        stage.setScene(scene);
        stage.show();
    }

    @SuppressWarnings("deprecation")
    private boolean loginTryAuthentication(Class<?> c) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<?> results = new ArrayList<User>();

        results = session.createCriteria(c).add(Restrictions.eq("login", firstNameTextField.getText()))
                .add(Restrictions.eq("password", encodedUserPassword(passwordTextField.getText()))).list();

        if (results.isEmpty() ) {

            errorAlert.setHeaderText("Niepoprawne dane logowania!");
            errorAlert.showAndWait();
        } else {
            if(((User)results.get(0)).getDeactivationDate() == null) {
                errorAlert = new Alert(AlertType.INFORMATION);
                errorAlert.setHeaderText("Zalogowano się!");
                loggedUser = results.get(0);
                errorAlert.showAndWait();
                stage.close();
                return true;
            }
            errorAlert.setHeaderText("Niepoprawne dane logowania!");
            errorAlert.showAndWait();
        }

        session.getTransaction().commit();
        session.close();
        return false;
    }

}
