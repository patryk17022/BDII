package pl.bdii.clinic.login;


import org.hibernate.Session;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.hibernate.SessionFactory;
import pl.bdii.util.HibernateUtil;

import java.util.Optional;

public class ClinicMain extends Application {

    LoginPageController loginController;

    /*
    Wymagane uzupełnienie na serwerze 2 Tabel:
        -examinations_dictionary -> PHYSICAL oraz LAB
        -specialites_dics

        ARGUMENTY: ADRES_BAZY LOGIN HASLO

     */

    public static void main(String[] args) {

        try {
            if (args.length == 3) {


                HibernateUtil.changeConfiguration(args[1], args[2], args[0]);
                launch(args);
                HibernateUtil.shutdown();
            } else {
                System.out.println("BLEDNA ILOSC ARGUMENTOW (WYMAGANE 3)");
                HibernateUtil.shutdown();
                return;
            }
        }catch (Exception ex)
        {
            System.out.println("ARGUMENTY SĄ NIEPORPAWNE: DATABASE LOGIN HASLO ");
            return;
        }

    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/bdii/clinic/login/loginPage.fxml"));
        Parent root = loader.load();
        loginController = loader.getController();

        loginController.showScene(root, stage);


        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.close();
        } catch (ExceptionInInitializerError ex) {
            Alert errorAlert = new Alert(AlertType.ERROR);
            errorAlert.setTitle("Database error");
            errorAlert.setHeaderText("Sprawdz połączenie z bazą danych !");
            errorAlert.setContentText(ex.getMessage());
            ex.printStackTrace();
            errorAlert.setResult(new ButtonType("Spróbuj ponownie", ButtonBar.ButtonData.LEFT));
            Optional<ButtonType> result = errorAlert.showAndWait();
            if (result.get() == ButtonType.OK) {

                System.exit(0);
            }
        }
    }

}
