/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabTechWindow;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.hibernate.Session;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.LabTechnician;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;


public class LabTechCancelExamination {

    private ExaminationLab exam;

    @FXML
    private Label idExam;

    public void ini(ExaminationLab exam) {
        this.exam = exam;
        idExam.setText("ID: " + Integer.toString(exam.getId()));
    }

    @FXML
    public void onClickNoButton(ActionEvent event) throws IOException {

        LabTechWindow lab = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml");
        lab.InitializeTable();
    }


    @FXML
    public void onClickYesButton(ActionEvent event) throws IOException {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        exam.setStatus(STATUS.BADANIA.CANCLED);
        LabTechnician user = (LabTechnician) LoginPageController.loggedUser;
        exam.setLabTech(user);
        session.update(exam);

        session.getTransaction().commit();
        session.close();

        LabTechWindow lab = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml");
        lab.InitializeTable();

    }

}