/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabTechWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.hibernate.Session;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.LabTechnician;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;

public class LabTechDoExamination {

    private ExaminationLab exam;

    @FXML
    private TextArea results;

    @FXML
    private TextArea doctorinfo;

    @FXML
    private Button cancleButton;

    @FXML
    private Button acceptButton;

    @FXML
    private Label info;

    public void ini(ExaminationLab exam) {
        this.exam = exam;
        doctorinfo.setText(exam.getDocAnn());
        info.setText("Badanie: " + exam.getId());
        results.setText(exam.getResult());

        if(!exam.getStatus().equals(STATUS.BADANIA.NEW))
        {
            acceptButton.setDisable(true);
            results.setEditable(false);

        }

        if(exam.getStatus().equals(STATUS.BADANIA.CANCLED) ||exam.getStatus().equals(STATUS.BADANIA.CONFIRMED))
        {
            cancleButton.setDisable(true);
        }
    }


    @FXML
    public void cancleExamination(ActionEvent event) throws IOException {
        ExaminationLab lab = exam;
        if (exam != null) {
            LabTechCancelExamination anu = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechCancle.fxml");
            anu.ini(lab);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie wybrano badania !");
            alert.showAndWait();
        }
    }

    @FXML
    public void performExamination(ActionEvent event) throws IOException {

        if (results.getText().length() > 3) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            exam.setStatus(STATUS.BADANIA.PERFORMED);
            LabTechnician user = (LabTechnician) LoginPageController.loggedUser;
            exam.setLabTech(user);
            exam.setResult(results.getText());

            session.update(exam);

            session.getTransaction().commit();
            session.close();

            LabTechWindow lab = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml");
            lab.InitializeTable();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie wprowadzono wynikow badania !");
            alert.showAndWait();
        }
    }


    @FXML
    public void backButton(ActionEvent event) throws IOException {

        LabTechWindow lab = LabTechWindow.ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml");
        lab.InitializeTable();
    }

}
