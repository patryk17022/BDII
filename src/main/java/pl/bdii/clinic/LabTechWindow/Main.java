package pl.bdii.clinic.LabTechWindow;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Session;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.LabTechnician;
import pl.bdii.util.HibernateUtil;

import java.util.List;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<LabTechnician> listaWizyt = session.createCriteria(LabTechnician.class).list();
        if (listaWizyt.size() > 0)
            LoginPageController.loggedUser = listaWizyt.get(0);

        session.getTransaction().commit();
        session.close();


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/pl/bdii/clinic/LabTechWindow/LabTechWindow.fxml"));
        Parent add_admin_page = (Parent) loader.load();
        ((LabTechWindow) loader.getController()).InitializeTable();
        primaryStage.setScene(new Scene(add_admin_page));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
        HibernateUtil.shutdown();
    }
}
