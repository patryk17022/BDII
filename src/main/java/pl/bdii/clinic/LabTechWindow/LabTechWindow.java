/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.bdii.clinic.LabTechWindow;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.entities.*;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import javax.xml.soap.Text;
import java.io.IOException;
import java.util.List;


public class LabTechWindow {

    @FXML
    private TableView table;

    @FXML
    private TextField idFil;

    @FXML
    private ComboBox typeFil;

    @FXML
    private ComboBox statusFil;


    private boolean init = false;

    public void filterButton(ActionEvent event)
    {


        InitializeTable();
    }

    public void initializeCombo(){
        init= true;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                .add(Restrictions.eq("type", "LAB")).list();

        ObservableList<String> dane = FXCollections.observableArrayList();

        dane.add("WSZYSTKIE");
        for (ExaminationDictionary entry : obiektyzBazy) {
            dane.add(entry.getName());

        }

        session.getTransaction().commit();
        session.close();


        typeFil.setItems(dane);
        typeFil.getSelectionModel().selectFirst();


        ObservableList<String> daneStatus = FXCollections.observableArrayList();

        daneStatus.add("WSZYSTKIE");
        daneStatus.add("NEW - NOWA");
        daneStatus.add("PER - PRZEPROWADZONA");
        daneStatus.add("CON - ZAKONCZONE");
        daneStatus.add("CAN - ANULOWANE");

        statusFil.setItems(daneStatus);
        statusFil.getSelectionModel().select(1);

    }

    public void InitializeTable() {

        if(!init)
            initializeCombo();

        TableColumn<ExaminationLab, Integer> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<ExaminationLab, Integer>("id"));

        TableColumn<ExaminationLab, String> docAnn = new TableColumn<>("Informacje od Doktora");
        docAnn.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("docAnn"));

        TableColumn<ExaminationLab, String> nameExam = new TableColumn<>("Typ badania");
        nameExam.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("nameExam"));

        TableColumn<ExaminationLab, String> Status = new TableColumn<>("Status");
        Status.setCellValueFactory(new PropertyValueFactory<ExaminationLab, String>("status"));

        table.getColumns().setAll(id, docAnn, nameExam,Status);

        ObservableList<ExaminationLab> data = FXCollections.observableArrayList();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationLab> list = session.createCriteria(ExaminationLab.class).list();

        for (ExaminationLab entry : list) {

            if(!statusFil.getSelectionModel().isSelected(0) && !entry.getStatus().equals(((String)(statusFil.getSelectionModel().getSelectedItem())).substring(0,3)))
                continue;

            if(!typeFil.getSelectionModel().isSelected(0) && !entry.getExDic().getName().equals(((String)(typeFil.getSelectionModel().getSelectedItem()))))
                continue;
            try {
                if (idFil.getText().length() != 0 && entry.getId() != Integer.parseInt(idFil.getText()))
                    continue;
            }catch(Exception ex)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Blad");
                alert.setContentText("Nie poprawne dane!");
                alert.showAndWait();
            }



            entry.setNameExam(entry.getExDic().getName());
            data.add(entry);
        }


        session.getTransaction().commit();
        session.close();

        table.setItems(data);

    }

    static public <T> T ChangeScene(ActionEvent event, String path) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Object.class.getClass().getResource(path));
            Parent add_admin_page = (Parent) loader.load();
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(new Scene(add_admin_page));
            app_stage.show();

            return ((T) loader.getController());
        } catch (Exception ex) {
        }
        return null;
    }

    @FXML
    public void performExamination(ActionEvent event) throws IOException {

        ExaminationLab lab = (ExaminationLab) table.getSelectionModel().getSelectedItem();
        if (lab != null ) {
            LabTechDoExamination wyk = ChangeScene(event, "/pl/bdii/clinic/LabTechWindow/LabTechPerform.fxml");
            wyk.ini(lab);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie wybrano badania !");
            alert.showAndWait();
        }

    }


    @FXML
    public void exBut(ActionEvent event) {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.close();
        HibernateUtil.shutdown();

    }


}
