package pl.bdii.clinic.DoctorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.clinic.DoctorWindow.TableEntity.VariousSurveysTable;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.util.List;

public class ExaminationWindow {


    @FXML
    TableView tableExam;

    private Visit visit;
    @FXML
    private Button buttonPhysical;
    @FXML
    private Button buttonLab;

    public void InitializeTableExam(Visit wizyta) {

        this.visit = wizyta;

        if (!wizyta.getStatus().equals(STATUS.WIZYTY.REGISTRED) ) {
            buttonPhysical.setDisable(true);
            buttonLab.setDisable(true);
        }

        //ustawienie kolumn
        TableColumn<VariousSurveysTable, String> id = new TableColumn<>("Numer Badania");
        id.setCellValueFactory(new PropertyValueFactory<VariousSurveysTable, String>("examinationNumber"));

        TableColumn<VariousSurveysTable, String> nazwaBadania = new TableColumn<>("Nazwa Badania");
        nazwaBadania.setCellValueFactory(new PropertyValueFactory<VariousSurveysTable, String>("examinationName"));

        TableColumn<VariousSurveysTable, String> typBadania = new TableColumn<>("Typ Badania");
        typBadania.setCellValueFactory(new PropertyValueFactory<VariousSurveysTable, String>("typBadania"));

        TableColumn<VariousSurveysTable, String> statusBad = new TableColumn<>("Status Badania");
        statusBad.setCellValueFactory(new PropertyValueFactory<VariousSurveysTable, String>("status"));


        tableExam.getColumns().setAll(id, nazwaBadania, typBadania,statusBad);

        ObservableList<VariousSurveysTable> listaBadan = FXCollections.observableArrayList();

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationLab> listaLab = session.createCriteria(ExaminationLab.class)
                .add(Restrictions.eq("visit", wizyta)).list();

        for (int i = 0; i < listaLab.size(); i++) {

            VariousSurveysTable badania = new VariousSurveysTable();
            badania.setTypBadania("LABORATORYJNE");
            badania.setExaminationName(listaLab.get(i).getExDic().getName());
            badania.setExaminationNumber(Integer.toString(listaLab.get(i).getId()));

            switch (listaLab.get(i).getStatus()) {
                case STATUS.BADANIA.CANCLED:
                    badania.setStatus("CANCLED");
                    break;
                case STATUS.BADANIA.CONFIRMED:
                    badania.setStatus("CONFIRMED");
                    break;
                case STATUS.BADANIA.NEW:
                    badania.setStatus("NEW");
                    break;
                case STATUS.BADANIA.PERFORMED:
                    badania.setStatus("PERFORMED");
                    break;
            }

            listaBadan.add(badania);

        }

        List<pl.bdii.entities.ExaminationPhysical> listafizykalne = session.createCriteria(pl.bdii.entities.ExaminationPhysical.class)
                .add(Restrictions.eq("visit", wizyta)).list();

        for (int i = 0; i < listafizykalne.size(); i++) {

            VariousSurveysTable badania = new VariousSurveysTable();
            badania.setTypBadania("FIZYKALNE");
            badania.setExaminationName(listafizykalne.get(i).getExDic().getName());
            badania.setExaminationNumber(Integer.toString(listafizykalne.get(i).getId()));
            badania.setStatus("CONFIRMED");
            listaBadan.add(badania);
        }


        session.getTransaction().commit();
        session.close();

        tableExam.setItems(listaBadan); //to będzie wyświetlane w tabeli


        System.out.println("Wypisano ExaminationWindow!");


    }


    @FXML
    public void physicalExamButton(ActionEvent event) throws IOException {

        DoExamPhysicialWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/DoExamPhysical.fxml");
        bad.ini(visit);

    }


    @FXML
    public void labExamButton(ActionEvent event) throws IOException {
        DoExamLabWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/DoExamLab.fxml");
        bad.ini(visit);

    }


    @FXML
    public void backToVisitButton(ActionEvent event) throws IOException {
        VisitsWindow wiz = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Visits.fxml");
        wiz.ini(visit);

    }

    @FXML
    public void detailsButton(ActionEvent event) throws IOException {

        VariousSurveysTable badania = (VariousSurveysTable) tableExam.getSelectionModel().getSelectedItem();

        if (badania != null) {
            if (badania.getTypBadania().equals("FIZYKALNE")) {

                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                List<pl.bdii.entities.ExaminationPhysical> listaLab = session.createCriteria(pl.bdii.entities.ExaminationPhysical.class)
                        .add(Restrictions.eq("id", Integer.parseInt(badania.getExaminationNumber()))).list();

                listaLab.get(0).getExDic().getName();
                session.getTransaction().commit();
                session.close();


                ExaminationPhysicalWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/ExaminationPhysical.fxml");
                bad.ini(visit, listaLab.get(0));


            } else {

                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                List<ExaminationLab> listaLab = session.createCriteria(ExaminationLab.class)
                        .add(Restrictions.eq("id", Integer.parseInt(badania.getExaminationNumber()))).list();

                listaLab.get(0).getExDic().getName();
                session.getTransaction().commit();
                session.close();


                ExaminationLabWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/LabExamination.fxml");
                bad.ini(visit, listaLab.get(0));
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano badania!");
            alert.showAndWait();
        }

    }

}
