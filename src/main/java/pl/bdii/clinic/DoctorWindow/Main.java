package pl.bdii.clinic.DoctorWindow;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Session;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.Doctor;
import pl.bdii.util.HibernateUtil;

import java.util.List;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Doctor> listaWizyt = session.createCriteria(Doctor.class).list();

        if(listaWizyt.size() > 0)
        LoginPageController.loggedUser = listaWizyt.get(0);
        session.getTransaction().commit();
        session.close();


        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/pl/bdii/clinic/DoctorWindow/DoctorWindow.fxml"));
        Parent add_doctor_page = (Parent) loader.load();
        ((DoctorWindow) loader.getController()).InitializeTable();
        primaryStage.setTitle("Panel Doktora");
        primaryStage.setScene(new Scene(add_doctor_page));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
        HibernateUtil.shutdown();
    }
}
