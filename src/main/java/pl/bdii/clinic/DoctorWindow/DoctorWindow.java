package pl.bdii.clinic.DoctorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.clinic.DoctorWindow.TableEntity.VisitsTable;
import pl.bdii.clinic.login.LoginPageController;
import pl.bdii.entities.Doctor;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class DoctorWindow {


    public Doctor doctor;

    @FXML
    TableView tableVisits;

    @FXML
    private TextField nameTB;

    @FXML
    private TextField surrnameTB;

    @FXML
    private TextField idTB;

    @FXML
    private TextField peselTB;

    @FXML
    private ChoiceBox statusTB;

    @FXML
    private DatePicker fromDP;

    @FXML
    private DatePicker toDP;

    private boolean init = false;

    private boolean filter = false;


    @FXML
    public void filterButton(ActionEvent event)
    {

        filter = true;
        InitializeTable();
    }

    private void initialiceComboBox(){
        init =true;

        ObservableList<String> daneStatus = FXCollections.observableArrayList();

        daneStatus.add("WSZYSTKIE");
        daneStatus.add("ZAREJESTROWANA");
        daneStatus.add("ZAKONCZONA");
        daneStatus.add("ANULOWANA");


        statusTB.setItems(daneStatus);
        statusTB.getSelectionModel().select(1);

        LocalDate today = LocalDate.now();
        toDP.setValue(today);
        fromDP.setValue(today);

    }

    private boolean isDateFromRange(DatePicker date1 , DatePicker date2, Date date)
    {

        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date dataact = formatter.parse(formatter.format(date));

            Date dateFrom = java.sql.Date.valueOf(date1.getValue());
            Date dateTo = java.sql.Date.valueOf(date2.getValue());
            if ((dataact.before(dateTo) && dataact.after(dateFrom)) || dataact.equals(dateTo) || dataact.equals(dateFrom))
                return true;
            else
                return false;
        }catch(Exception ex){
            return false;
        }
    }

    public void InitializeTable() {

        doctor = (Doctor) LoginPageController.loggedUser;


        if(!init)
            initialiceComboBox();

        //ustawienie kolumn
        TableColumn<VisitsTable, String> id = new TableColumn<>("Numer Wizyty");
        id.setCellValueFactory(new PropertyValueFactory<VisitsTable, String>("visitNumber"));

        TableColumn<VisitsTable, String> data = new TableColumn<>("Data");
        data.setCellValueFactory(new PropertyValueFactory<VisitsTable, String>("date"));

        TableColumn<VisitsTable, String> imie = new TableColumn<>("Imie Pacjenta");
        imie.setCellValueFactory(new PropertyValueFactory<VisitsTable, String>("firstname"));

        TableColumn<VisitsTable, String> nazwisko = new TableColumn<>("Nazwisko Pacjenta");
        nazwisko.setCellValueFactory(new PropertyValueFactory<VisitsTable, String>("secondname"));

        TableColumn<VisitsTable, String> pesel = new TableColumn<>("PESEL Pacjenta");
        pesel.setCellValueFactory(new PropertyValueFactory<VisitsTable, String>("pesel"));

        TableColumn<Visit, Date> planAnulDate = new TableColumn<>("Data Zmiany Statusu");
        planAnulDate.setCellValueFactory(new PropertyValueFactory<Visit, Date>("planCancleDT"));

        TableColumn<Visit, String> StatusWizyty = new TableColumn<>("Status");
        StatusWizyty.setCellValueFactory(new PropertyValueFactory<Visit, String>("status"));


        tableVisits.getColumns().setAll(id, data, imie, nazwisko, pesel,StatusWizyty,planAnulDate);

        ObservableList<VisitsTable> lista = FXCollections.observableArrayList();


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Visit> listaWizyt = session.createCriteria(Visit.class)
                .add(Restrictions.eq("doctor", doctor)).list();

        for (int i = 0; i < listaWizyt.size(); i++) {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            VisitsTable elementWizyty = new VisitsTable();
            elementWizyty.setVisitNumber(Integer.toString(listaWizyt.get(i).getId()));
            elementWizyty.setDate(df.format(listaWizyt.get(i).getVisitDate().getTime()));
            elementWizyty.setFirstname(listaWizyt.get(i).getPatient().getFirstName());
            elementWizyty.setSecondname(listaWizyt.get(i).getPatient().getLastName());
            elementWizyty.setPesel(listaWizyt.get(i).getPatient().getPesel());
            if(listaWizyt.get(i).getPlanCancleDT() != null)
              elementWizyty.setPlanCancleDT(df.format(listaWizyt.get(i).getPlanCancleDT()));
            if(listaWizyt.get(i).getPlanCancleDT()!= null)
                elementWizyty.setDateaction(df.format(listaWizyt.get(i).getPlanCancleDT().getTime()));

            switch (listaWizyt.get(i).getStatus()) {
                case STATUS.WIZYTY.CANCLED:
                    elementWizyty.setStatus("ANULOWANA");
                    break;
                case STATUS.WIZYTY.ENDED:
                    elementWizyty.setStatus("ZAKONCZONA");
                    break;
                case STATUS.WIZYTY.REGISTRED:
                    elementWizyty.setStatus("ZAREJESTROWANA");
                    break;
            }


            if (nameTB.getText().length() != 0 && !elementWizyty.getFirstname().toUpperCase().matches("^"+nameTB.getText().toUpperCase()+".*"))
                continue;
            if (surrnameTB.getText().length() != 0 && !elementWizyty.getSecondname().toUpperCase().matches("^"+surrnameTB.getText().toUpperCase()+".*"))
                continue;

            if (peselTB.getText().length() != 0 && !elementWizyty.getPesel().toUpperCase().matches("^"+peselTB.getText().toUpperCase()+"[0-9]*"))
                continue;

            try {
                if (idTB.getText().length() != 0 && !elementWizyty.getVisitNumber().equals(idTB.getText()))
                    continue;
            }catch(Exception ex)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setContentText("Nie poprawne dane!");
                alert.setTitle("Blad");
                alert.showAndWait();
            }

            String status = ((String)(statusTB.getSelectionModel().getSelectedItem()));
            if(!statusTB.getSelectionModel().isSelected(0) && !elementWizyty.getStatus().equals(status))
                continue;

            if(filter && !isDateFromRange(fromDP,toDP,listaWizyt.get(i).getVisitDate()))
                continue;

            lista.add(elementWizyty);
        }
        session.getTransaction().commit();
        session.close();

        tableVisits.setItems(lista); //to będzie wyświetlane w tabeli

        System.out.println("Wypisano użytkowników!");

    }





    @FXML
    public void visit(ActionEvent event) throws IOException {

        VisitsTable wizyta = (VisitsTable) tableVisits.getSelectionModel().getSelectedItem();
        if (wizyta != null) {

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Visit> listaWizyt = session.createCriteria(Visit.class)
                    .add(Restrictions.eq("id", Integer.parseInt(wizyta.getVisitNumber()))).list();

            listaWizyt.get(0).getPatient().getFirstName();
            listaWizyt.get(0).getDoctor().getFirstName();

            session.getTransaction().commit();
            session.close();

            VisitsWindow wiz = ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Visits.fxml");
            wiz.ini(listaWizyt.get(0));

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano wizyty!");
            alert.showAndWait();
        }


    }


    @FXML
    public void exit(ActionEvent actionEvent) {
        Stage app_stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        app_stage.close();
        HibernateUtil.shutdown();
    }

    static public <T> T ChangeScene(ActionEvent event, String path) {
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(Object.class.getClass().getResource(path));
            Parent add_admin_page = (Parent) loader.load();
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(new Scene(add_admin_page));
            app_stage.show();

            return ((T) loader.getController());
        } catch (Exception ex) {
        }
        return null;
    }


}
