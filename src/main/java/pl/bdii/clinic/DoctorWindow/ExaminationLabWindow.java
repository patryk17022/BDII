package pl.bdii.clinic.DoctorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.Visit;

import java.io.IOException;

public class ExaminationLabWindow {


    private Visit visit;
    private ExaminationLab examinationLab;
    @FXML
    private TextArea docAnn;

    @FXML
    private TextArea labManAnn;

    @FXML
    private TextArea examResults;

    @FXML
    private Label examType;

    public void ini(Visit wizyta, ExaminationLab examinationLab) {

        this.visit = wizyta;
        this.examinationLab = examinationLab;
        docAnn.setText(examinationLab.getDocAnn());
        labManAnn.setText(examinationLab.getLabManAnn());
        examResults.setText(examinationLab.getResult());
        examType.setText(examinationLab.getExDic().getName());

    }

    @FXML
    public void backToExamButton(ActionEvent event) throws IOException {
        ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
        bad.InitializeTableExam(visit);
    }

}
