package pl.bdii.clinic.DoctorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import pl.bdii.clinic.DoctorWindow.TableEntity.VisitsTable;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.util.Date;

public class VisitsWindow {

    @FXML
    private Button interButton;

    @FXML
    private TextField firstname;

    @FXML
    private TextField secondname;

    @FXML
    private TextField pesel;

    @FXML
    private TextArea diagnose;

    @FXML
    private TextArea visitDesc;

    @FXML
    private Button cancleButton;


    private Visit visitActual;

    public void ini(Visit wizytaNumer) {

        visitActual = wizytaNumer;
        firstname.setText(wizytaNumer.getPatient().getFirstName());
        secondname.setText(wizytaNumer.getPatient().getLastName());
        pesel.setText(wizytaNumer.getPatient().getPesel());
        diagnose.setText(wizytaNumer.getDiagnose());
        visitDesc.setText(wizytaNumer.getDesc());

        if (!visitActual.getStatus().equals(STATUS.WIZYTY.REGISTRED)) {
            interButton.setDisable(true);
            cancleButton.setDisable(true);
            diagnose.setEditable(false);
            visitDesc.setEditable(false);
        }


    }

    @FXML
    public void cancleVisit(ActionEvent event) throws IOException {

        Visit wizyta = visitActual;
        if (wizyta != null) {

            CancleVisit anuluj = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/CancleVisit.fxml");
            anuluj.setIdVisit(visitActual.getId());

        }
    }

    @FXML
    public void performInterview(ActionEvent event) throws IOException {


        if (visitDesc.getText() != null &&diagnose.getText()!= null&& visitDesc.getText().length() > 3 && diagnose.getText().length() > 3) {

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            visitActual.setDiagnose(diagnose.getText());
            visitActual.setDesc(visitDesc.getText());
            visitActual.setStatus(STATUS.WIZYTY.ENDED);
            visitActual.setPlanCancleDT(new Date());

            session.update(visitActual);

            session.getTransaction().commit();
            session.close();

            VisitsWindow wiz = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Visits.fxml");
            wiz.ini(visitActual);
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Należy uzupełnić pola diagnoza oraz opis (minimum 4 znaki)!");
            alert.showAndWait();
        }

    }


    @FXML
    public void examinationButton(ActionEvent event) throws IOException {
        ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
        bad.InitializeTableExam(visitActual);
    }


    @FXML
    public void erlierVisitButton(ActionEvent event) throws IOException {
        EarlierVisitsWindow wczewiz = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/EarlierVisit.fxml");
        wczewiz.InitializeEarlierVisitTab(visitActual);

    }


    @FXML
    public void backButton(ActionEvent event) throws IOException {

        DoctorWindow win = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/DoctorWindow.fxml");
        win.InitializeTable();

    }


}

