package pl.bdii.clinic.DoctorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.clinic.DoctorWindow.TableEntity.EarlierVisitsTable;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class EarlierVisitsWindow {


    private Visit visit;

    @FXML
    TableView earlierVisitTab;

    public void InitializeEarlierVisitTab(Visit wizyta) {


        this.visit = wizyta;

        TableColumn<EarlierVisitsTable, String> id = new TableColumn<>("Numer Wizyty");
        id.setMinWidth(100);
        id.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("visitNumber"));

        TableColumn<EarlierVisitsTable, String> data = new TableColumn<>("Data");
        data.setMinWidth(75);
        data.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("data"));

        TableColumn<EarlierVisitsTable, String> imielekarza = new TableColumn<>("Imie Lekarza");
        imielekarza.setMinWidth(100);
        imielekarza.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("firstDoctorName"));

        TableColumn<EarlierVisitsTable, String> nazwiskoLekarza = new TableColumn<>("Nazwisko Lekarza");
        nazwiskoLekarza.setMinWidth(110);
        nazwiskoLekarza.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("surrnnameDoctor"));

        TableColumn<EarlierVisitsTable, String> diagnoza = new TableColumn<>("Diagnoza");
        diagnoza.setMinWidth(75);
        diagnoza.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("diagnoza"));

        TableColumn<EarlierVisitsTable, String> statusWizyty = new TableColumn<>("Status Wizyty");
        statusWizyty.setMinWidth(100);
        statusWizyty.setCellValueFactory(new PropertyValueFactory<EarlierVisitsTable, String>("statusVisit"));


        earlierVisitTab.getColumns().setAll(id, data, imielekarza, nazwiskoLekarza, diagnoza, statusWizyty);

        ObservableList<EarlierVisitsTable> listaWczesniejszychWizyt = FXCollections.observableArrayList();


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Visit> listaWizyt = session.createCriteria(Visit.class)
                .add(Restrictions.eq("patient", wizyta.getPatient()))
                .add(Restrictions.ne("status", STATUS.WIZYTY.REGISTRED)).list();

        for (int i = 0; i < listaWizyt.size(); i++) {

            EarlierVisitsTable element = new EarlierVisitsTable();
            DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

            element.setVisitNumber(Integer.toString(listaWizyt.get(i).getId()));
            element.setData(df.format(listaWizyt.get(i).getRegDate().getTime()));
            element.setFirstDoctorName(listaWizyt.get(i).getDoctor().getFirstName());
            element.setFirstDoctorName(listaWizyt.get(i).getDoctor().getLastName());
            element.setDiagnoza(listaWizyt.get(i).getDiagnose());


            if (listaWizyt.get(i).getStatus().equals(STATUS.WIZYTY.CANCLED)) {
                element.setStatusVisit("ANULOWANA");
            } else {
                element.setStatusVisit("ZAKOŃCZONA");
            }

            listaWczesniejszychWizyt.add(element);
        }

        session.getTransaction().commit();
        session.close();


        earlierVisitTab.setItems(listaWczesniejszychWizyt); //to będzie wyświetlane w tabeli


        System.out.println("Wypisano wcześniejsze badania!");


    }


    @FXML
    public void backToVisitButton(ActionEvent event) throws IOException {
        VisitsWindow wiz = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Visits.fxml");
        wiz.ini(visit);
    }

    @FXML
    public void detailsButton(ActionEvent event) throws IOException {


        EarlierVisitsTable wizyta = (EarlierVisitsTable) earlierVisitTab.getSelectionModel().getSelectedItem();
        if (wizyta != null) {

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            List<Visit> listaWizyt = session.createCriteria(Visit.class)
                    .add(Restrictions.eq("id", Integer.parseInt(wizyta.getVisitNumber()))).list();

            listaWizyt.get(0).getDoctor().getFirstName();
            listaWizyt.get(0).getPatient().getFirstName();


            session.getTransaction().commit();
            session.close();

            VisitsWindow wiz = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Visits.fxml");
            wiz.ini(listaWizyt.get(0));

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano wizyty!");
            alert.showAndWait();
        }


    }

}
