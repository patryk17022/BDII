package pl.bdii.clinic.DoctorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.entities.ExaminationDictionary;
import pl.bdii.entities.ExaminationPhysical;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;


import java.io.IOException;
import java.util.List;

public class DoExamPhysicialWindow {

    private Visit visit;

    @FXML
    private TextArea examResult;

    @FXML
    private ComboBox examType;


    public void ini(Visit wizyta) {

        this.visit = wizyta;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                .add(Restrictions.eq("type", "PHYSICAL")).list();

        ObservableList<String> dane = FXCollections.observableArrayList();

        for (ExaminationDictionary entry : obiektyzBazy) {
            dane.add(entry.getId() + " " + entry.getName());

        }

        examType.setItems(dane);
        examType.getSelectionModel().selectFirst();

        session.getTransaction().commit();
        session.close();
    }

    @FXML
    public void confirmButton(ActionEvent event) throws IOException {

        try {
            if (examResult.getText().length() > 3) {

                ExaminationPhysical examinationPhysical = new ExaminationPhysical();
                examinationPhysical.setResult(examResult.getText());
                examinationPhysical.setVisit(visit);

                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();

                String wybranyTyp = (String) examType.getSelectionModel().getSelectedItem();

                int idTypuBadania = Integer.parseInt(wybranyTyp.substring(0, wybranyTyp.indexOf(' ')));

                List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                        .add(Restrictions.eq("id", idTypuBadania)).list();

                examinationPhysical.setExDic(obiektyzBazy.get(0));
                session.save(examinationPhysical);

                session.getTransaction().commit();
                session.close();


                ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
                bad.InitializeTableExam(visit);

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Blad");
                alert.setContentText("Nie wprowadzono danych!");
                alert.showAndWait();
            }
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano typu!");
            alert.showAndWait();
        }
    }

    @FXML
    public void backToExaminationButton(ActionEvent event) throws IOException {

        ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
        bad.InitializeTableExam(visit);
    }

}
