package pl.bdii.clinic.DoctorWindow.TableEntity;

public class VariousSurveysTable {

    private String examinationNumber;
    private String examinationName;
    private String typBadania;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExaminationNumber() {
        return examinationNumber;
    }

    public void setExaminationNumber(String examinationNumber) {
        this.examinationNumber = examinationNumber;
    }

    public String getExaminationName() {
        return examinationName;
    }

    public void setExaminationName(String examinationName) {
        this.examinationName = examinationName;
    }

    public String getTypBadania() {
        return typBadania;
    }

    public void setTypBadania(String typBadania) {
        this.typBadania = typBadania;
    }
}
