package pl.bdii.clinic.DoctorWindow.TableEntity;

public class EarlierVisitsTable {

    private String visitNumber;
    private String data;
    private String firstDoctorName;
    private String surrnnameDoctor;
    private String diagnoza;
    private String statusVisit;


    public String getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFirstDoctorName() {
        return firstDoctorName;
    }

    public void setFirstDoctorName(String firstDoctorName) {
        this.firstDoctorName = firstDoctorName;
    }

    public String getSurrnnameDoctor() {
        return surrnnameDoctor;
    }

    public void setSurrnnameDoctor(String surrnnameDoctor) {
        this.surrnnameDoctor = surrnnameDoctor;
    }

    public String getDiagnoza() {
        return diagnoza;
    }

    public void setDiagnoza(String diagnoza) {
        this.diagnoza = diagnoza;
    }

    public String getStatusVisit() {
        return statusVisit;
    }

    public void setStatusVisit(String statusVisit) {
        this.statusVisit = statusVisit;
    }
}
