package pl.bdii.clinic.DoctorWindow.TableEntity;

public class VisitsTable {

    private String pesel;
    private String firstname;
    private String secondname;
    private String visitNumber;
    private String date;
    private String status;
    private String dateaction;
    private String planCancleDT;

    public String getPlanCancleDT() {
        return planCancleDT;
    }

    public void setPlanCancleDT(String planCancleDT) {
        this.planCancleDT = planCancleDT;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateaction() {
        return dateaction;
    }

    public void setDateaction(String dateaction) {
        this.dateaction = dateaction;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
