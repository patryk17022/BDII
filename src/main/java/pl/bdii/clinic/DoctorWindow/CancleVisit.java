package pl.bdii.clinic.DoctorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class CancleVisit {


    private int idVisit;

    @FXML
    private Label idLabel;

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
        idLabel.setText("ID: " + idVisit);
    }

    @FXML
    public void cancleVisit(ActionEvent event) throws IOException {

        DoctorWindow doc = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/DoctorWindow.fxml");
        doc.InitializeTable();

    }


    @FXML
    public void back(ActionEvent event) throws IOException {

        System.out.println("Anulowanie wizyty! Numer wizyty to: " + idVisit);


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Visit> listaWizyt = session.createCriteria(Visit.class)
                .add(Restrictions.eq("id", idVisit)).list();

        Visit wizyta = listaWizyt.get(0);
        wizyta.setStatus(STATUS.WIZYTY.CANCLED);


        wizyta.setPlanCancleDT(new Date());

        session.update(wizyta);

        session.getTransaction().commit();
        session.close();


        DoctorWindow doc = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/DoctorWindow.fxml");
        doc.InitializeTable();
    }

}
