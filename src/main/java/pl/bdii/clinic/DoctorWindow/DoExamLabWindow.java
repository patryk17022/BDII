package pl.bdii.clinic.DoctorWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import pl.bdii.entities.ExaminationDictionary;
import pl.bdii.entities.ExaminationLab;
import pl.bdii.entities.Visit;
import pl.bdii.util.HibernateUtil;
import pl.bdii.util.STATUS;

import java.io.IOException;
import java.util.List;

public class DoExamLabWindow {

    private Visit visit;

    @FXML
    private TextArea docAnn;

    @FXML
    private ComboBox examType;


    public void ini(Visit wizyta) {
        this.visit = wizyta;

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                .add(Restrictions.eq("type", "LAB")).list();

        ObservableList<String> dane = FXCollections.observableArrayList();


        for (ExaminationDictionary entry : obiektyzBazy) {
            dane.add(entry.getId() + " " + entry.getName());

        }

        session.getTransaction().commit();
        session.close();


        examType.setItems(dane);
        examType.getSelectionModel().selectFirst();

    }

    @FXML
    public void confirmButton(ActionEvent event) throws IOException {

        ExaminationLab examinationLab = new ExaminationLab();
        examinationLab.setStatus(STATUS.BADANIA.NEW);
        examinationLab.setDocAnn(docAnn.getText());
        examinationLab.setVisit(visit);

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            String wybranyTyp = (String) examType.getSelectionModel().getSelectedItem();

            int idTypuBadania = Integer.parseInt(wybranyTyp.substring(0, wybranyTyp.indexOf(' ')));

            List<ExaminationDictionary> obiektyzBazy = session.createCriteria(ExaminationDictionary.class)
                    .add(Restrictions.eq("id", idTypuBadania)).list();

            examinationLab.setExDic(obiektyzBazy.get(0));
            session.save(examinationLab);

            session.getTransaction().commit();
            session.close();

            ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
            bad.InitializeTableExam(visit);
        }catch(Exception ex)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad");
            alert.setContentText("Nie wybrano typu!");
            alert.showAndWait();
        }
    }

    @FXML
    public void backToExaminationButton(ActionEvent event) throws IOException {

        ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
        bad.InitializeTableExam(visit);
    }

}
