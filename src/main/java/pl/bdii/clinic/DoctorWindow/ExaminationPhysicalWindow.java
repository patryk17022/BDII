package pl.bdii.clinic.DoctorWindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import pl.bdii.entities.ExaminationPhysical;
import pl.bdii.entities.Visit;

import java.io.IOException;

public class ExaminationPhysicalWindow {


    private Visit visit;
    private ExaminationPhysical examinationPhysical;

    @FXML
    private TextArea examDesc;

    @FXML
    private Label examType;

    public void ini(Visit wizyta, pl.bdii.entities.ExaminationPhysical examinationPhysical) {

        this.visit = wizyta;
        this.examinationPhysical = examinationPhysical;
        examDesc.setText(examinationPhysical.getResult());
        examType.setText(examinationPhysical.getExDic().getName());
    }


    @FXML
    public void backToExamButton(ActionEvent event) throws IOException {
        ExaminationWindow bad = DoctorWindow.ChangeScene(event, "/pl/bdii/clinic/DoctorWindow/Examination.fxml");
        bad.InitializeTableExam(visit);
    }


}
