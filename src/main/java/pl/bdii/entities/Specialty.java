package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SPECIALITIES_DICT")
public class Specialty {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_SPEC")
    private int id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "specialty")
    private List<Doctor> doctors;

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Specialty(int id, String n) {
        setIdReg(id);
        setName(n);
    }

    public Specialty() {
        setIdReg(0);
        setName("");
    }

    public int getId() {
        return id;
    }

    public void setIdReg(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
