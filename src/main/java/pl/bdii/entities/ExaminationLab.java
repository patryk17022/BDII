package pl.bdii.entities;

import javax.persistence.*;

@Entity
@Table(name = "LAB_EXAMINATIONS")
public class ExaminationLab {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_EXL")
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Visit visit;
	
	@Column(name = "Doctor_Annotations")
	private String docAnn;
	private String result;
	
	@Column(name = "LabMan_Annotations")
	private String labManAnn;
	private String status;

	@Transient
	private String nameExam;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private ExaminationDictionary exDic;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private LabTechnician labTech;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private LabManager labMan;

	public ExaminationLab() {
		super();
		this.visit = null;
		this.docAnn = "";
		this.result = "";
		this.labManAnn = "";
		this.status = "";
		this.exDic = null;
		this.labTech = null;
		this.labMan = null;
	}

	public ExaminationLab(Visit visit, String docAnn, String result, String labManAnn, String status,
						  ExaminationDictionary exDic, LabTechnician labTech, LabManager labMan) {
		super();
		this.visit = visit;
		this.docAnn = docAnn;
		this.result = result;
		this.labManAnn = labManAnn;
		this.status = status;
		this.exDic = exDic;
		this.labTech = labTech;
		this.labMan = labMan;
	}

	public String getNameExam() {
		return nameExam;
	}

	public void setNameExam(String nameExam) {
		this.nameExam = nameExam;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public String getDocAnn() {
		return docAnn;
	}

	public void setDocAnn(String docAnn) {
		this.docAnn = docAnn;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getLabManAnn() {
		return labManAnn;
	}

	public void setLabManAnn(String labAnn) {
		this.labManAnn = labAnn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public ExaminationDictionary getExDic() {
		return exDic;
	}

	public void setExDic(ExaminationDictionary exDic) {
		this.exDic = exDic;
	}

	public LabTechnician getLabTech() {
		return labTech;
	}

	public void setLabTech(LabTechnician labTech) {
		this.labTech = labTech;
	}

	public LabManager getLabMan() {
		return labMan;
	}

	public void setLabMan(LabManager labMan) {
		this.labMan = labMan;
	}

}
