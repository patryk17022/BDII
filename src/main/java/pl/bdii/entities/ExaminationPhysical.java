package pl.bdii.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PHYSICAL_EXAMINATIONS")
public class ExaminationPhysical {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_EXP")
    private int id;

    private String result;

    @ManyToOne(fetch = FetchType.LAZY)
    private ExaminationDictionary exDic;

    @ManyToOne(fetch = FetchType.LAZY)
    private Visit visit;

    public ExaminationPhysical() {
        super();
        this.result = "";
        this.exDic = null;
        this.visit = null;
    }

    public ExaminationPhysical(String result, ExaminationDictionary exDic, Visit visit) {
        super();
        this.result = result;
        this.exDic = exDic;
        this.visit = visit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ExaminationDictionary getExDic() {
        return exDic;
    }

    public void setExDic(ExaminationDictionary exDic) {
        this.exDic = exDic;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

}
