package pl.bdii.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ADDRESS")
public class Adres {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_ADR")
    private int id;

    private String city;

    private String streetName;

    private String postalCode;

    private int houseNumber;

    private int flatNumber;

    @OneToMany(mappedBy = "adres")
    private List<Patient> patient;

    public List<Patient> getPatient() {
        return patient;
    }

    public void setPatient(List<Patient> patient) {
        this.patient = patient;
    }

    public Adres(String city, String streetName, String postalCode, int houseNumber, int flatNumber, List<Patient> patient) {
        super();
        this.city = city;
        this.streetName = streetName;
        this.postalCode = postalCode;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.patient = patient;
    }

    public Adres() {
        super();
        this.city = "";
        this.streetName = "";
        this.postalCode = "";
        this.houseNumber = 0;
        this.flatNumber = 0;
        this.patient = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

}
