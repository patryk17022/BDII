package pl.bdii.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "VISITS")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_VIS")
    private int id;

    @Column(name = "DESCRIPTION")
    private String desc;

    private String diagnose;

    private String status;

    @Column(name = "VISIT_DATE")
    private Date visitDate;

    @Column(name = "REGISTRATION_DATE", nullable = false)
    private Date regDate;

    @Column(name = "PLAN_CANCLE_DT")
    private Date planCancleDT;


    @ManyToOne(fetch = FetchType.LAZY)
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.LAZY)
    private Patient patient;

    @OneToMany(mappedBy = "visit")
    private List<ExaminationLab> exLab;

    @OneToMany(mappedBy = "visit")
    private List<ExaminationPhysical> exPhys;

    @Transient
    public String statusName;

    @Transient
    public String imieNazwiskoLekarza;

    public Visit() {
        super();
        this.desc = "";
        this.diagnose = "";
        this.status = "";
        this.visitDate = null;
        this.regDate = null;
        this.doctor = null;
        this.patient = null;
        this.exLab = null;
        this.exPhys = null;
        this.planCancleDT = null;
    }

    public Visit(String desc, String diagnose, String status, Date visitDate, Date regDate, Doctor doctor,
                 Patient patient, List<ExaminationLab> exLab, List<ExaminationPhysical> exPhys, Date planCancleDT) {
        super();
        this.desc = desc;
        this.diagnose = diagnose;
        this.status = status;
        this.visitDate = visitDate;
        this.regDate = regDate;
        this.doctor = doctor;
        this.patient = patient;
        this.exLab = exLab;
        this.exPhys = exPhys;
        this.planCancleDT = planCancleDT;
    }

    public Date getPlanCancleDT() {
        return planCancleDT;
    }

    public void setPlanCancleDT(Date planCancleDT) {
        this.planCancleDT = planCancleDT;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getImieNazwiskoLekarza() {
        return imieNazwiskoLekarza;
    }

    public void setImieNazwiskoLekarza(String imieNazwiskoLekarza) {
        this.imieNazwiskoLekarza = imieNazwiskoLekarza;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<ExaminationLab> getExLab() {
        return exLab;
    }

    public void setExLab(List<ExaminationLab> exLab) {
        this.exLab = exLab;
    }

    public List<ExaminationPhysical> getExPhys() {
        return exPhys;
    }

    public void setExPhys(List<ExaminationPhysical> exPhys) {
        this.exPhys = exPhys;
    }

}
