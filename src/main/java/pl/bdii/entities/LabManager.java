package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LAB_MANAGERS")
public class LabManager extends User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_MAN")
    private int id;

    @OneToMany(mappedBy = "labMan")
    private List<ExaminationLab> exLabs;

    public LabManager(String fN, String lN, String pass, String log) {
        super(fN, lN, pass, log);

    }

    public List<ExaminationLab> getExLabs() {
        return exLabs;
    }

    public void setExLabs(List<ExaminationLab> exLabs) {
        this.exLabs = exLabs;
    }

    public LabManager() {
        super();
        setId(0);
    }

    public int getId() {
        return id;
    }

    public void setId(int idMan) {
        this.id = idMan;
    }

}
