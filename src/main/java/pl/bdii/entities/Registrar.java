package pl.bdii.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REGISTRARS")
public class Registrar extends User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_REG")
    private int id;

    public Registrar(String lN, String fN, String pass, String log) {
        super(fN, lN, pass, log);

    }

    public Registrar() {
        super();
        setId(0);
    }

    public int getId() {
        return id;
    }

    public void setId(int idReg) {
        this.id = idReg;
    }

}
