package pl.bdii.entities;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.Date;

@MappedSuperclass
public class User {

    @Column(name = "FIRST_NAME", nullable = false)
    protected String firstName;

    @Column(name = "LAST_NAME", nullable = false)
    protected String lastName;

    @Column(name = "PASSWORD", nullable = false)
    protected String password;

    @Column(name = "LOGIN", unique = true, nullable = false)
    protected String login;

    @Transient
    protected String rola;

    @Column(name = "DEACTIVATION_DATE")
    protected Date deactivationDate;

    public Date getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public String getRola() {
        return rola;
    }

    public void setRola(String rola) {
        this.rola = rola;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public User(String fN, String lN, String pas, String login) {
        setFirstName(fN);
        setLastName(lN);
        setPassword(pas);
        this.login = login;
        deactivationDate = null;
    }

    public User() {
        setFirstName("");
        setLastName("");
        setPassword("");
        this.login = "";
        deactivationDate = null;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
