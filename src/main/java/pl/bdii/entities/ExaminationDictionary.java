package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EXAMINATIONS_DICTIONARY")
public class ExaminationDictionary {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_EXDIC")
    private int id;

    private String type;

    private String name;

    @OneToMany(mappedBy = "exDic")
    private List<ExaminationLab> exLabs;

    @OneToMany(mappedBy = "exDic")
    private List<ExaminationPhysical> exPsych;

    public ExaminationDictionary() {
        super();
        this.type = "";
        this.name = "";
        this.exLabs = null;
        this.exPsych = null;
    }

    public ExaminationDictionary(String type, String name, List<ExaminationLab> exLabs,
                                 List<ExaminationPhysical> exPsych) {
        super();
        this.type = type;
        this.name = name;
        this.exLabs = exLabs;
        this.exPsych = exPsych;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ExaminationLab> getExLabs() {
        return exLabs;
    }

    public void setExLabs(List<ExaminationLab> exLabs) {
        this.exLabs = exLabs;
    }

    public List<ExaminationPhysical> getExPsych() {
        return exPsych;
    }

    public void setExPsych(List<ExaminationPhysical> exPsych) {
        this.exPsych = exPsych;
    }

}
