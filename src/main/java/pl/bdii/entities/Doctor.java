package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DOCTORS")
public class Doctor extends User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_DOC")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Specialty specialty;

    @OneToMany(mappedBy = "doctor")
    private List<Visit> visits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public Doctor(Specialty spe, String fN, String lN, String pas, String log) {
        super(fN, lN, pas, log);
        specialty = spe;
    }

    public Doctor() {
        super();
        setIdDoc(0);
    }

    public int getIdDoc() {
        return id;
    }

    public void setIdDoc(int idDoc) {
        this.id = idDoc;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

}
