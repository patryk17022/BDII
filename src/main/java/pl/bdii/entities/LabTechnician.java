package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LAB_TECHNICIANS")
public class LabTechnician extends User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_TECH")
    private int id;

    @OneToMany(mappedBy = "labTech")
    private List<ExaminationLab> exLabs;

    public List<ExaminationLab> getExLabs() {
        return exLabs;
    }

    public void setExLabs(List<ExaminationLab> exLabs) {
        this.exLabs = exLabs;
    }

    public LabTechnician(String fN, String lN, String pass, String log) {
        super(fN, lN, pass, log);

    }

    public LabTechnician() {
        super();
        setId(0);
    }

    public int getId() {
        return id;
    }

    public void setId(int idTech) {
        this.id = idTech;
    }

}
