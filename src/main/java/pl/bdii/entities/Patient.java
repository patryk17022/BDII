package pl.bdii.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PATIENTS")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_PAT")
    private int id;

    private String firstName;

    private String lastName;

    private String pesel;

    private String UbezpiecznieNumber;

    public String getUbezpiecznieNumber() {
        return UbezpiecznieNumber;
    }

    public void setUbezpiecznieNumber(String ubezpiecznieNumber) {
        UbezpiecznieNumber = ubezpiecznieNumber;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Adres adres;

    @OneToMany(mappedBy = "patient")
    private List<Visit> visits;

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public Patient() {
        super();
        this.firstName = "";
        this.lastName = "";
        this.pesel = "";
        this.adres = null;
        this.UbezpiecznieNumber = "";
    }

    public Patient(String firstName, String lastName, String pesel, Adres adres, String nrUb) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.adres = adres;
        this.UbezpiecznieNumber = nrUb;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }
}
