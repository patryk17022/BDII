package pl.bdii.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    public static void changeConfiguration(String login, String password, String database) throws Exception{
        Configuration cfg = new Configuration();
        cfg.configure();
        cfg.setProperty("hibernate.connection.password", password);
        cfg.setProperty("hibernate.connection.username", login);
        cfg.setProperty("hibernate.connection.url", "jdbc:postgresql://"+database);

        sessionFactory = cfg.buildSessionFactory();

    }


    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null)
            sessionFactory = buildSessionFactory();

        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

    protected void finalize() throws Throwable {
        try {
            HibernateUtil.shutdown();
        } finally {
            super.finalize();
        }
    }

}